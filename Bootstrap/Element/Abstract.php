<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic interface for bootstrap element file
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Bootstrap_Element_Abstract
implements Infrastructure_Bootstrap_Element_Interface
{
    /**
     * Stores the instance of current bootstrap
     * 
     * @var Zend_Application_Bootstrap_Bootstrap
     */
    private $_bootstrap;
    
    public function __construct(
        Zend_Application_Bootstrap_Bootstrap $bootstrap,
        $options = array()
    )
    {
        $this->_bootstrap = $bootstrap;
        $this->setOptions($options);
    }
    
    /**
     * Sets the options
     * 
     * @param array $options
     * @return Infrastructure_Bootstrap_Element_Abstract
     */
    public function setOptions($options)
    {
        $isCorrectOptions = is_array($options);
            assert($isCorrectOptions);
        
        foreach ($options as $optionIndex => $optionContent) {
            $property = '_' . $optionIndex;
            if (property_exists($this, $property)) {
                $this->$property = $optionContent;
            }
        }
        
        return $this;
    }
    
    /**
     * Returns the instance of bootstrap
     * 
     * @return Zend_Application_Bootstrap_Bootstrap
     */
    public function getBootstrap()
    {
        return $this->_bootstrap;
    }
}
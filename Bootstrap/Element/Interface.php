<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for iniciate the the element in the bootstrap file
 * 
 * @category Infrastructure
 * @package Infrastructure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Bootstrap_Element_Interface
{
    /**
     * @param Zend_Application_Bootstrap_Bootstrap $bootstrap
     * @param array $options
     */
    public function __construct(
        Zend_Application_Bootstrap_Bootstrap $bootstrap,
        $options = array()
    );
    
    /**
     * Sets the options
     * 
     * @param array $options
     * @return Infrastructure_Bootstrap_Element_Interface
     */
    public function setOptions($options);
    
    /**
     * Returns the instance of bootstrap
     * 
     * @return Zend_Application_Bootstrap_Bootstrap
     */
    public function getBootstrap();
    
    /**
     * Initiates the element
     */
    public function initiate();
}
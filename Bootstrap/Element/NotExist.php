<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Infrastructure_Bootstrap_Element_NotExist
 * 
 * Exception for behavior of not exist bootstrap element
 * 
 * @category Infrastructure
 * @package Infrastructure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Bootstrap_Element_NotExist
extends Infrastructure_Exception_Base
{
    
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Bootstrap
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Makes the instance of bootstrap element
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Bootstrap
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Bootstrap_Factory
extends Infrastructure_Factory
{
    /**
     * Stores the instance of current factory
     * 
     * @var Infrastructure_Bootstrap_Factory
     */
    private static $_instance;
    
    /**
     * Stores the instance of parent exception class
     * 
     * @var Exception
     */
    private $_parentException;
    
    public static function getInstance()
    {
        parent::getInstance();
        
        if (!is_object(self::$_instance)) {
            $getCurrentClass = get_called_class();
            self::$_instance = new $getCurrentClass();
        }
        
        return self::$_instance;
    }
    
    public function make($resourceName, $parentClass, $options = array())
    {
        $this->_setResourceName($resourceName);
        $this->_setParentClass(get_class($parentClass));
        
        try {
            $getResourceClassNameResult = $this->_getResourceClassName();
            $resourceClassInstance = new $getResourceClassNameResult(
                $parentClass
            );
            
            return $resourceClassInstance;
        } catch (Exception $ex) {
            $this->_parentException = $ex;
            throw $this->_getException();
        }
    }
    
    protected function _getResourceClassName()
    {
        $getResourceName = $this->_getResourceName();
        $getParentClassNameResult = $this->_getParentClass();
        $sliceParentClass = explode('_', $getParentClassNameResult);
        $resourceClassName = $sliceParentClass[0] . '_'
            . $sliceParentClass[1] . '_Element_' . $getResourceName;
        
        return $resourceClassName;
    }
    
    protected function _getException()
    {
        $getResourceName = $this->_getResourceName();
        
        return new Infrastructure_Bootstrap_Element_NotExist(
            'Bootstrap element of ' . $getResourceName . ' and class '
            . $this->_getResourceClassName() . ' not exist! '
            . $this->_parentException->getMessage()
        );
    }
}
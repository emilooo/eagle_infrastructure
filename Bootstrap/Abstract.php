<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Bootstrap
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic interface for bootstrap element file
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Bootstrap
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Bootstrap_Abstract
extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * Instance of zend module autoloader
     * 
     * @var Zend_Application_Module_Autoloader
     */
    protected $_resourceLoader;
    
    /**
     * Instance of zend logger
     * 
     * @var Zend_Log
     */
    protected $_logger;
    
    /**
     * @var Zend_Controller_Front
     */
    public $frontController;
    
    /**
     * Returns the instance of element to initiate through the bootstrap
     * 
     * @param string $elementName Name of element
     * @param array $options
     * @return Infrastructure_Bootstrap_Element_Abstract
     */
    public function getElement($elementName, $options = array())
    {
        $bootstrapFactory = Infrastructure_Bootstrap_Factory::getInstance();
        $bootstrapElement = $bootstrapFactory->make(
            $elementName, $this, $options
        );
        
        return $bootstrapElement;
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Action
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Creates the instance of action. This instance has generated
 * with name of controller
 * 
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Action
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Action_Factory
extends Infrastructure_Factory
{
    private static $_instance;
    
    /**
     * Stores the instance of parent exception class
     * 
     * @var Exception
     */
    private $_parentException;
    
    public static function getInstance()
    {
        if (!is_object(self::$_instance)) {
            $getCurrentClass = get_called_class();
            self::$_instance = new $getCurrentClass();
        }
        
        return self::$_instance;
    }
    
    public function make($resourceName, $parentClass, $options = array())
    {
        $this->_setResourceName($resourceName);
        $this->_setParentClass(get_class($parentClass));
        
        try {
            $getResourceClassNameResult = $this->_getResourceClassName();
            $resourceClassInstance = new $getResourceClassNameResult(
                $parentClass
            );
            
            return $resourceClassInstance;
        } catch (Exception $ex) {
            $this->_parentException = $ex;
            throw $this->_getException();
        }
    }
    
    protected function _getResourceClassName()
    {
        $getResourceName = $this->_getResourceName();
        $getParentClassNameResult = $this->_getParentClass();
        $sliceParentClass = explode('_', $getParentClassNameResult);
        $resourceClassName = $sliceParentClass[0] . '_Action_'
            . $sliceParentClass[1] . '_' . $getResourceName;
        
        return $resourceClassName;
    }
    
    protected function _getException()
    {
        $getResourceName = $this->_getResourceName();
        
        return new Infrastructure_Action_NotExist(
            'Action of ' . $getResourceName . ' and class '. $this->_getResourceClassName() . ' not exist! '
            . $this->_parentException->getMessage()
        );
    }
}

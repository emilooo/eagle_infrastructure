<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Action
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for actions to use controller
 * 
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Action
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Action_Interface
{
    /**
     * Prepares settings of action
     */
    public function init();
    
    /**
     * Sets the options of class
     * 
     * @param array $options
     */
    public function setOptions($options);
    
    /**
     * Retruns the result of action
     * 
     * @return boolean
     */
    public function makeAction();
}
<?php

abstract class Infrastructure_Action_Http_Error_Abstract
extends Infrastructure_Action_Http_Abstract
{
    public function __construct(
        \Infrastructure_Controller_Http_Error_Interface $controller,
        $options = array()
    ) {
        parent::__construct($controller, $options);
    }
    
    public function init()
    {
        parent::init();
        
        $this->_prepareToRender();
    }
    
    /**
     * Returns the content header for file view
     * 
     * @return string
     */
    abstract protected function _getContentHeader();
    
    /**
     * Returns the content message for file view
     * 
     * @return string
     */
    abstract protected function _getContentMessage();
    
    protected function _prepareToRender($messages = null)
    {
        $contentHeader = $this->_getContentHeader();
        $contentMessage = $this->_getContentMessage();
        
        $this->getController()->view->content = array(
            'header' => $contentHeader,
            'message' => $contentMessage
        );
        $this->getController()->view->error
            = $this->getController()->getException();
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for http actions
 * 
 * @category Infrastructure
 * @package Infrastructure_Action
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Action_Http_Interface
extends Infrastructure_Action_Interface
{
    /**
     * Adds the page title
     * 
     * @param Zend_View_Helper_HeadTitle $headTitleHelper Instance of zend
     * layout helper
     * @return void
     */
    public function appendToPageTitle(
        Zend_View_Helper_HeadTitle $headTitleHelper
    );
    
    /**
     * Adds the page meta tags
     * 
     * @param Zend_View_Helper_HeadMeta $headMetaHelper Instance of head meta
     * @return void
     */
    public function appendToPageMetaTags(
        Zend_View_Helper_HeadMeta $headMetaHelper
    );
    
    /**
     * Adds the page css styles
     * 
     * @param Zend_View_Helper_HeadStyle $headStyleHelper Instance of head style
     * @return void
     */
    public function appendToPageStyles(
        Zend_View_Helper_HeadStyle $headStyleHelper
    );
    
    /**
     * Adds the page java scripts
     * 
     * @param Zend_View_Helper_HeadScript $headScriptHelper Instance
     * of head script
     * @return void
     */
    public function appendToPageScripts(
        Zend_View_Helper_HeadScript $headScriptHelper
    );
    
    /**
     * Adds the page template
     * 
     * @param Zend_View_Helper_Layout $layoutHelper Instance
     * of template layout helper
     * @return void
     */
    public function appendToTemplate(
        Zend_View_Helper_Layout $layoutHelper
    );
    
    /**
     * Adds the page links
     * 
     * @param Zend_View_Helper_HeadLink $headLinkHelper Instance
     * of link helper
     * @return void
     */
    public function appendToPageLinks(
        Zend_View_Helper_HeadLink $headLinkHelper
    );
    
    /**
     * Adds the page breadcrumbs
     */
    public function appendToPageBreadcrumbs();
}

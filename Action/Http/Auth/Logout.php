<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for logout action of http controller. This action
 * logged of user
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Auth_Logout
extends Infrastructure_Action_Http_Auth_Abstract
{
    /**
     * Stores the message for correct process of logout
     * 
     * @var string
     */
    const CORRECTLOGOUT = "MessageForCorrectLogoutProcess";
    
    /**
     * Stores the message for invalid process of logout
     * 
     * @var string
     */
    const INVALIDLOGOUT = "MessageForIncorrectLogoutProcess";
    
    /**
     * Stores the instance of authentication service
     * 
     * @var Core_Service_Authentication
     */
    private $_authenticationService;
    
    /**
     * Stores the instance of flash messenger helper
     * 
     * @var Zend_Controller_Action_Helper_FlashMessenger 
     */
    protected $_flashHelper;
    
    public function init()
    {
        $this->_authenticationService = $this->_getAuthenticationService();
        $this->_flashHelper
            = $this->getController()->getHelper('FlashMessenger');
    }
    
    public function makeAction()
    {
        if ($this->_authenticationService->getIdentity()
                instanceof Zend_Db_Table_Row_Abstract) {
            $this->_authenticationService->clear();
            if (empty($this->_authenticationService->getIdentity())) {
                $this->_flashHelper->addMessage(self::CORRECTLOGOUT);
                return true;
            } else {
                $this->_flashHelper
                    ->addMessage(self::INVALIDLOGOUT);
                return false;
            }
        }
        
        return false;
    }
    
    /**
     * Returns the instace of authentication service
     */
    abstract protected function _getAuthenticationService();
    
    protected function _prepareToRender($messages = null)
    {
    }
}
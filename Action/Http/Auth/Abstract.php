<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for actions that using http controller
 * of auth
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Auth_Abstract
extends Infrastructure_Action_Http_Abstract
{
    public function __construct(
        \Infrastructure_Controller_Http_Auth_Interface $controller,
        $options = array())
    {
        parent::__construct($controller, $options);
    }
}
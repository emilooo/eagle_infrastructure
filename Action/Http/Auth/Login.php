<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for login action of http controller. This action
 * has logging user through data of post
 * 
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Auth_Login
extends Infrastructure_Action_Http_Auth_Abstract
{
    /**
     * Stores the message for correct process of login. This message is
     * showing when user put correct data to login
     * 
     * @var string
     */
    const CORRECTLOGIN = 'MessageForCorrectLoginProcess';
    
    /**
     * Stores the message for invalid process of login. This message is
     * showing when process of login ended is fail
     */
    const INVALIDLOGGIN = 'MessageForInvalidLoginProcess';
    
    /**
     * Instance of http controller
     * 
     * @var Infrastructure_Controller_Http_Auth_Abstract
     */
    private $_controller;
    
    /**
     * Stores the instance of view
     * 
     * @var Zend_View
     */
    protected $_view;
    
    /**
     * Stores the data from post
     * 
     * @var array
     */
    protected $_postData;
    
    /**
     * Stores the instance of service authentication
     * 
     * @var Core_Service_Authentication
     */
    protected $_authenticationService;
    
    /**
     * Stores the instance of flash messenger helper
     * 
     * @var Zend_Controller_Action_Helper_FlashMessenger 
     */
    protected $_flash;
    
    /**
     * Stores the login form
     * 
     * @var Infrastructure_Form_Abstract
     */
    protected $_form;
    
    public function __construct(
        Infrastructure_Controller_Http_Auth_Abstract $controller,
        $options = array()
    )
    {
        parent::__construct($controller);
    }
    
    public function init()
    {
        parent::init();
        
        $this->_view = $this->getController()->view;
        $this->_postData = $this->getController()->getRequest()->getPost();
        $this->_authenticationService = $this->_getAuthenticationService();
        $this->_form = $this->_getForm();
        $this->_flash
            = $this->getController()->getHelper('FlashMessenger');
    }
    
    public function makeAction()
    {
        if (!empty($this->_postData)) {
            if (!$this->getController()->isAuthorizedRequest()) {
                $this->_prepareToRender(
                    array('This request is unauthorized please try again!')
                );
                return false;
            }
            
            if ($this->_form->isValid($this->_postData)) {
                if ($this->_doExecute()) {
                    $this->_flash->addMessage(self::CORRECTLOGIN);
                    $this->_view->messages = $this->_flash->getMessages();
                    return true;
                } else {
                    $this->_prepareToRender(array(self::INVALIDLOGGIN));
                    return false;
                }
            }
        }
        
        $this->_prepareToRender($this->_flash->getMessages());
        return false;
    }
    
    /**
     * Executes authentication process through data of post
     * 
     * @return boolean
     */
    private function _doExecute()
    {
        return $this->_authenticationService->authenticate(
            array(
                'email'=>$this->_postData['email'],
                'password'=>$this->_postData['password']
            )
        );
    }
    
    /**
     * Prepares the view to render
     */
    protected function _prepareToRender($messages = null)
    {
        $isCorrectMessage = is_array($messages);
            assert($isCorrectMessage);
        $this->_form->populate($this->_postData);
        $this->_form->setAction(
            $this->getController()->getRequest()->getRequestUri()
        );
        $this->_view->form = $this->_form;
        $this->_view->messages = $messages;
    }
    
    /**
     * Returns the instance of form
     * 
     * @return Infrastructure_Form_Interface
     */
    abstract protected function _getForm();
    
    /**
     * Returns the instace of authentication service
     */
    abstract protected function _getAuthenticationService();
}
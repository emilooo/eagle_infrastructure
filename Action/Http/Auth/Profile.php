<?php
/**
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for profile action of http controller. This action
 * has showing the information about user
 * 
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Auth_Profile
extends Infrastructure_Action_Http_Auth_Abstract
{
    /**
     * Instance of http controller
     * 
     * @var Infrastructure_Controller_Http_Auth_Abstract
     */
    private $_controller;
    
    /**
     * Stores the instance of view
     * 
     * @var Zend_View
     */
    protected $_view;
    
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * @var string
     */
    protected $_modelName;
    
    /**
     * Stores the information about current login of user from request
     * 
     * @var string
     */
    protected $_login;
    
    public function __construct(
        Infrastructure_Controller_Http_Auth_Abstract $controller,
        $options = array()
    )
    {
        parent::__construct($controller);
    }
    
    public function init()
    {
        parent::init();
        
        $this->_view = $this->getController()->view;
        $this->_model = $this->getController()->getModel();
        $this->_modelName = $this->_model->getResourceId();
        $this->_login = $this->getController()->getRequest()->getParam('login');
    }
    
    public function makeAction()
    {
        try {
            $readOneItemResult = $this->_doExecute();
            if (!empty($readOneItemResult)) {
                $this->_view->record = $readOneItemResult;
                return true;
            }
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            return false;
        }
        
        return false;
    }
    
    protected function _prepareToRender($messages = null)
    {
    }
    
    private function _doExecute()
    {
        return $this->_model->getCached($this->_modelName)
            ->readOneItemByLogin($this->_login);
    }
}
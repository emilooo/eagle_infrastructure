<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for register action of http controller. This action
 * has register the new user through data of post
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Auth_Register
extends Infrastructure_Action_Http_Auth_Abstract
{
    /**
     * Stores the message for correct process of register. This message is
     * showing when process of register ended is done
     * 
     * @var string
     */
    const CORRECT_REGISTER = 'MessageForCorrectRegisterProcess';
    
    /**
     * Stores the message for invalid process of register. This message is
     * showing when process of register ended is fail
     */
    const INVALID_REGISTER = 'MessageForInvalidRegisterProcess';
    
    /**
     * Instance of http controller
     * 
     * @var Infrastructure_Controller_Http_Auth_Abstract
     */
    private $_controller;
    
    /**
     * Stores the instance of view
     * 
     * @var Zend_View
     */
    protected $_view;
    
    /**
     * Stores the data from post
     * 
     * @var array
     */
    protected $_postData;
    
    /**
     * Stores the instance of service authentication
     * 
     * @var Core_Service_Authentication
     */
    protected $_authenticationService;
    
    /**
     * Stores the instance of flash messenger helper
     * 
     * @var Zend_Controller_Action_Helper_FlashMessenger 
     */
    protected $_flash;
    
    /**
     * Stores the register form
     * 
     * @var Infrastructure_Form_Abstract
     */
    protected $_registerForm;
    
    /**
     * Stores the instance of model of crud
     * 
     * @var Infrastructure_Model_Crud_Abstract
     */
    protected $_crudModel;
    
    public function init()
    {
        parent::init();
        
        $this->_view = $this->getController()->view;
        $this->_postData = $this->getController()->getRequest()->getPost();
        $this->_authenticationService = $this->_getAuthenticationService();
        $this->_flash
            = $this->getController()->getHelper('FlashMessenger');
        $this->_crudModel = $this->getController()->getModel();
        $this->_registerForm = $this->_crudModel->getForm('Register');
    }
    
    public function appendToPageBreadcrumbs()
    {
    }
    
    public function appendToPageLinks(
        \Zend_View_Helper_HeadLink $headLinkHelper
    )
    {
        $headLinkHelper->setStylesheet(
            '/css/application/templates/login/screen.css'
        );
    }
    
    public function appendToPageMetaTags(
        \Zend_View_Helper_HeadMeta $headMetaHelper
    )
    {
    }
    
    public function appendToPageScripts(
        \Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
    }
    
    public function appendToPageStyles(
        \Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function appendToPageTitle(
        \Zend_View_Helper_HeadTitle $headTitleHelper
    )
    {
    }
    
    public function appendToTemplate(
        \Zend_View_Helper_Layout $layoutHelper
    )
    {
        $layoutHelper->layout()->setLayout('login');
    }
    
    public function makeAction()
    {
        if (!empty($this->_postData)) {
            if (!$this->getController()->isAuthorizedRequest()) {
                $this->_prepareToRender(
                    array('This request is unauthorized please try again!')
                );
                return false;
            }
            
            $createItemResult
                = $this->_crudModel->registerItem($this->_postData);
            if ($createItemResult) {
                $this->_flash->addMessage(self::CORRECT_REGISTER);
                return true;
            }
            
            $messages = array(self::INVALID_REGISTER);
            $this->_prepareToRender($messages);
            return false;
        }
        
        $this->_prepareToRender();
        return false;
    }
    
    /**
     * Returns the instace of authentication service
     */
    abstract protected function _getAuthenticationService();
    
    /**
     * Prepares the data to render
     * 
     * @param array $messages
     */
    protected function _prepareToRender($messages = array())
    {
        $this->_registerForm->populate($this->_postData);
        $this->_view->form = $this->_registerForm;
        $this->_view->messages
            = (empty($messages)) ? $this->_flash->getMessages() : $messages;
    }
}


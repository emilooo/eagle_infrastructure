<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for actions that using controller of http
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Abstract
extends Infrastructure_Action_Abstract
implements Infrastructure_Action_Http_Interface
{
    public function __construct(
        \Infrastructure_Controller_Http_Interface $controller,
        $options = array()
    )
    {
        parent::__construct($controller, $options);
    }
    
    public function init()
    {
        $this->appendToPageTitle(
            $this->getController()->view->getHelper('HeadTitle')
        );
        $this->appendToPageMetaTags(
            $this->getController()->view->getHelper('HeadMeta')
        );
        $this->appendToPageStyles(
            $this->getController()->view->getHelper('HeadStyle')
        );
        $this->appendToPageScripts(
            $this->getController()->view->getHelper('HeadScript')
        );
        $this->appendToTemplate(
            $this->getController()->view->getHelper('Layout')
        );
        $this->appendToPageLinks(
            $this->getController()->view->getHelper('HeadLink')
        );
        $this->appendToPageBreadcrumbs();
        $this->_prepareRenderer();
    }
    
    /**
     * Prepares the name of view file to render. If result of method
     * _getViewRendererName is null then this method creates the filename
     * from name of action and name of controller without extension such, as:
     * 
     * Filename example:
     * action-controller.tpl
     * 
     * If result of method _getViewRendererName is not null then this method
     * creates the filename from result of method of _getViewRendererName.
     * 
     * @return void
     */
    protected function _prepareRenderer()
    {
        $getViewRendererName = $this->_getViewRendererName();
        if (empty($getViewRendererName)) {
            $controllerName = $this->getController()->getRequest()
                ->getControllerName();
            $actionName = $this->getController()->getRequest()
                ->getActionName();
            $getViewRendererName
                = strtolower($actionName) . "-" . strtolower($controllerName);
        }
        
        $viewRenderer = $this->getController()->getHelper('ViewRenderer')
            ->setRender(
                $getViewRendererName
            );
    }
    
    /**
     * Returns the name of renderer view filename without extenstion
     * 
     * @return string
     */
    abstract protected function _getViewRendererName();
    
    /**
     * Vierifies if user has access to action
     * 
     * @return boolean
     */
    public function hasAccessToAction()
    {
        return true;
    }
    
    /**
     * Executes when access to action is denied
     */
    public function doWhenAccessToActionIsDenied()
    {
    }
    
    /**
     * Executes when action has maked successfully
     */
    public function doWhenActionSuccessfullyMaked()
    {
    }
    
    /**
     * Executes when action has maked unsuccessfully
     */
    public function doWhenActionUnsuccessfullyMaked()
    {
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for read action of http controller. This action
 * reading the resource from crud model.
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Crud_Read
extends Infrastructure_Action_Http_Crud_Abstract
{
    /**
     * Stores the instance of view
     * 
     * @var Zend_View
     */
    protected $_view;
    
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * Stores the number of page
     * 
     * @var int
     */
    protected $_id;
    
    /**
     * @var string
     */
    protected $_modelName;
    
    public function init()
    {
        parent::init();
        
        $this->_view = $this->getController()->view;
        $this->_model = $this->getController()->getModel();
        $this->_id = $this->getController()->getRequest()->getParam('id');
        $this->_modelName = $this->_model->getResourceId();
    }
    
    public function makeAction()
    {
        try {
            $readOneItemResult = $this->_doExecute();
            if (!empty($readOneItemResult)) {
                $this->_view->record = $readOneItemResult;
                return true;
            }
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            return false;
        }
        
        return false;
    }
    
    protected function _prepareToRender($messages = null)
    {
    }
    
    private function _doExecute()
    {
        return $this->_model->getCached($this->_modelName)
            ->readOneItem($this->_id);
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for delete action of http controller. This action
 * deleting the resources.
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Crud_Delete
extends Infrastructure_Action_Http_Crud_Abstract
{
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * Stores the number of id
     * 
     * @var int
     */
    protected $_id;
    
    /**
     * Stores the name of model
     * 
     * @var string
     */
    private $_modelName;
    
    public function init()
    {
        parent::init();
        
        $this->_model = $this->getController()->getModel();
        $this->_id = $this->getController()->getRequest()->getParam('id');
        $this->_modelName = $this->_model->getResourceId();
    }
    
    /**
     * Returns the result of DeleteResourceTask adapter
     * 
     * @return boolean
     */
    public function makeAction()
    {
        try {
            return $this->_doExecute();
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            return false;
        }
    }
    
    /**
     * Deletes the item
     * 
     * @return boolean
     */
    protected function _doExecute()
    {
        $deleteItemResult = ($this->_model->deleteItem($this->_id) > 0);
        $this->_model->cleanCached($this->_modelName);
        
        return $deleteItemResult;
    }
    
    protected function _prepareToRender($messages = null)
    {
    }
}
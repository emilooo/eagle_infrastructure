<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for index action of http controller. This action
 * reading the resource from crud model.
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Crud_Index
extends Infrastructure_Action_Http_Crud_Abstract
{
    /**
     * Stores the instance of view
     * 
     * @var Zend_View
     */
    protected $_view;
    
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * Returns the number of page
     * 
     * @var int
     */
    protected $_page;
    
    /**
     * Stores the instance of flash messenger
     * 
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flash;
    
    /**
     * @var string
     */
    private $_modelName;
    
    /**
     * @param Infrastructure_Controller_Http_Crud_Interface $controller
     * @param array $options
     */
    public function __construct(
        Infrastructure_Controller_Http_Crud_Interface $controller,
        $options = array())
    {
        parent::__construct($controller, $options);
    }
    
    public function init()
    {
        parent::init();
        
        $this->_view = $this->getController()->view;
        $this->_model = $this->getController()->getModel();
        $this->_page = $this->getController()->getRequest()->getParam('page');
        $this->_flash
            = $this->getController()->getHelper('FlashMessenger');
        $this->_modelName = $this->_model->getResourceId();
    }
    
    /**
     * Returns true when resources exist or returns
     * false when resources not exist
     * 
     * @return boolean
     */
    public function makeAction()
    {
        $getRecordsResult = $this->_doExecute();
        $this->_prepareToRender();
        if (!empty($getRecordsResult)) {
            $this->_view->records = $getRecordsResult;
            return true;
        }
        
        return false;
    }
    
    protected function _prepareToRender($messages = null)
    {
        $this->_view->messages = $this->_flash->getMessages();
    }
    
    private function _doExecute()
    {
        $readManyItemsResult = $this->_model->getCached($this->_modelName)
            ->readManyItems($this->_page);
        
        return $readManyItemsResult;
    }
}
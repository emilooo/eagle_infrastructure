<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for create action of http controller. This action
 * creating the new resources or set the flash message about result of operation
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Crud_Create
extends Infrastructure_Action_Http_Crud_Abstract
{
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     *
     * @var type 
     */
    protected $_flash;
    
    /**
     * Stores the data from post
     * 
     * @var array
     */
    protected $_postData;
    
    /**
     * Stores the instance of create form
     * 
     * @var Zend_Form
     */
    protected $_form;
    
    /**
     * Stores the content of flash message
     * 
     * @var string
     */
    protected $_flashMessage;
    
    /**
     * Stores the name of model
     * 
     * @var string
     */
    private $_modelName;
    
    /**
     * @param Infrastructure_Controller_Http_Crud_Interface $controller
     * @param array $options
     */
    public function __construct(
        Infrastructure_Controller_Http_Crud_Interface $controller,
        $options = array())
    {
        parent::__construct($controller, $options);
    }
    
    public function init()
    {
        parent::init();
        
        $this->_model = $this->getController()->getModel();
        $this->_flash
            = $this->getController()->getHelper('FlashMessenger');
        $this->_postData = $this->getController()->getRequest()->getPost();
        $this->_form = $this->_getForm();
        $this->_flashMessage = 'MessageForCorrectCreateProcess';
        $this->_modelName = $this->_model->getResourceId();
    }
    
    public function makeAction()
    {
        $getPostDataResult = $this->_postData;
            if (!empty($getPostDataResult)) {
                $this->_form->populate($this->_postData);
                if ($this->_doExecute()) {
                    $this->_flash->addMessage($this->_flashMessage);
                    $this->_model->cleanCached($this->_modelName);
                    return true;
                }
            }
        $this->_prepareToRender();
        return false;
    }
    
    /**
     * Executes the operation
     * 
     * @return boolean
     */
    protected function _doExecute()
    {
        $getPostDataResult = $this->_postData;
        $createItemResult = $this->_model->createItem($getPostDataResult);
        
        return $createItemResult;
    }
    
    /**
     * Prepares the instance of form and instance of view to render
     * 
     * @return void
     */
    protected function _prepareToRender($messages = null)
    {
        $this->_form->setAction('create');
        $this->getController()->view->form = $this->_form;
        $this->getController()->view->messages = $this->getController()
            ->getHelper('FlashMessenger')->getMessages();
    }
    
    /**
     * Returns the instance of form
     * 
     * @return Infrastructure_Form_Interface
     */
    abstract protected function _getForm();
}
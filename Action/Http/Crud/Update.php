<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for update action of http controller. This action
 * updating the exist resource.
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Http_Crud_Update
extends Infrastructure_Action_Http_Crud_Abstract
{
    /**
     * Stores the instance of view
     * 
     * @var Zend_View
     */
    protected $_view;
    
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * Stores the instance of flash messenger
     * 
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flash;
    
    /**
     * Stores the instance of form
     * 
     * @var Zend_Form
     */
    protected $_form;
    
    /**
     * Stores the data of post
     * 
     * @var array
     */
    protected $_postData;
    
    /**
     * Stores the id
     * 
     * @var int
     */
    protected $_id;
    
    /**
     * @var boolean
     */
    protected $_isPost;
    
    /**
     * Stores the name of model
     * 
     * @var string
     */
    private $_modelName;
    
    public function init()
    {
        parent::init();
        
        $this->_model = $this->getController()->getModel();
        $this->_form = $this->_getForm();
        $this->_postData = $this->getController()->getRequest()->getPost();
        $this->_id = $this->getController()->getRequest()->getParam('id');
        $this->_isPost = $this->getController()->getRequest()->isPost();
        $this->_view = $this->getController()->view;
        $this->_flash
            = $this->getController()->getHelper('FlashMessenger');
        $this->_modelName = $this->_model->getResourceId();
    }
    
    /**
     * @return boolean
     * @todo You must creates the other class to delegate features
     */
    public function makeAction()
    {
        if ($this->_isPost) {
            if ($this->_doExecute()) {
                $this->_model->cleanCached($this->_modelName);
                return true;
            }
        }
        
        return $this->_prepareToRender();
    }
    
    protected function _doExecute()
    {
        return $this->_model->updateItem($this->_id, $this->_postData);
    }
    
    protected function _prepareToRender($messages = null)
    {
        if ($this->_isPost) {
            $this->_prepareToRenderWithDataFromPost();
            return false;
        }

        $this->_prepareToRenderWithDataFromDatabase();
        return false;
    }
    
    /**
     * Returns the instance of form
     * 
     * @return Infrastructure_Form_Interface
     */
    abstract protected function _getForm();
    
    /**
     * Prepares the view to render with data from post
     * 
     * @return void
     */
    private function _prepareToRenderWithDataFromPost()
    {
        $this->_form->populate($this->_postData);
        $this->_form->setAction($this->_generateFormAction());
        $this->_view->form = $this->_form;
        $this->_view->messages = $this->_flash->getMessages();
    }
    
    /**
     * Prepares the view to render with data from database
     * 
     * @return void
     */
    private function _prepareToRenderWithDataFromDatabase()
    {
        $readOneItemResult = $this->_model->readOneItem($this->_id);
        $this->_form->setAction($this->_generateFormAction());
        
        $this->_form->populate(
            $this->_model->toArrayWithoutColumnName(
                $readOneItemResult->toArray()
            )
        );
        $this->_view->form = $this->_form;
    }
    
    /**
     * Returns the form action
     * 
     * @return string
     */
    private function _generateFormAction()
    {
        $getModuleNameResult
            = $this->getController()->getRequest()->getModuleName();
        $getControllerNameResult
            = $this->getController()->getRequest()->getControllerName();
        $getActionNameResult
            = $this->getController()->getRequest()->getActionName();
        $getIdResult = $this->_id;
        return $formAction = '/' . $getModuleNameResult . '/'
            . $getControllerNameResult . '/' . $getActionNameResult . '/'
            . 'id/' . $getIdResult;
    }
}
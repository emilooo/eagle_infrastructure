<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Action
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for actions that using controller
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Action
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Abstract
implements Infrastructure_Action_Interface
{
    /**
     * Instance of http controller
     * 
     * @var Infrastructure_Controller_Http_Abstract
     */
    private $_controller;
    
    public function __construct(
        Infrastructure_Controller_Interface $controller,
        $options = array()
    )
    {
        $this->_controller = $controller;
        $this->init();
        $this->setOptions($options);
    }
    
    public function setOptions($options = array())
    {
        $isCorrectOptions = is_array($options);
            assert($isCorrectOptions);
        
        foreach ($options as $propertyName => $propertyValue) {
            $convertedPropertyName = '_' . $propertyName;
            if (property_exists($this, $convertedPropertyName)) {
                $this->$convertedPropertyName = $propertyValue;
            }
        }
        
        return $this;
    }
    
    /**
     * Returns the instance of controller
     * 
     * @return Infrastructure_Controller_Http_Abstract
     */
    public function getController()
    {
        return $this->_controller;
    }
    
    /**
     * Prepare the action to render
     * 
     * @param string $messages
     * @return void
     */
    abstract protected function _prepareToRender($messages = null);
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for actions to use controller of rest
 * with auth
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Auth_Abstract
extends Infrastructure_Action_Rest_Crud_Abstract
{
    public function __construct(
    \Infrastructure_Controller_Rest_Auth_Interface $controller,
        $options = array())
    {
        parent::__construct($controller, $options);
    }
}
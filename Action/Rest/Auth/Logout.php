<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for implements the rules for logout
 * user from system
 * 
 * Steps:
 *  1. Verify if user has logged
 *  2. Logout user
 *  3. Generate the response message for section of body
 *  4. Generate the http response code
 * 
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Auth_Logout
extends Infrastructure_Action_Rest_Auth_Abstract
{
    /**
     * Stores the instance of authentication service
     * 
     * @var Core_Service_Authentication
     */
    private $_authorizeService;
    
    /**
     * Stores the anauthorize user process
     * 
     * @var boolean
     */
    private $_anauthorizeResult;
    
    public function init()
    {
        $this->_authorizeService = new Core_Service_Authentication();
    }
    
    public function makeAction()
    {
        $this->_anauthorizeResult = $this->_anauthorizeUser();
        $this->_prepareToRender();
        $this->_prepareHeader();
        
        return $this->_anauthorizeResult;
    }
    
    public function getResponse()
    {
        return $this->_response;
    }
    
    protected function _prepareToRender($messages = null)
    {
        $response = array(
            'status' => ($this->_anauthorizeResult) ? 200 : 406,
            'errors' => null
        );
        
        return $this->_response = json_encode($response);
    }
    
    protected function _prepareHeader()
    {
        if ($this->_anauthorizeResult) {
            $this->getController()->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getController()->getResponse()->setHttpResponseCode(406);
        }
    }
    
    protected function _anauthorizeUser()
    {
        if ($this->_authorizeService->getIdentity() != false) {
            $this->_authorizeService->clear();
            return true;
        }
        
        return false;
    }
}
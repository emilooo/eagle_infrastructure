<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for login action of http controller. This action
 * has logging user through data of post
 * 
 * Steps:
 *  1. Authenticate user
 *  2. Generates the response message
 *  3. Generates the header
 *  4. Sets the request params
 * 
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Auth_Login
extends Infrastructure_Action_Rest_Auth_Abstract
{
    /**
     * Stores the data from post
     * 
     * @var array
     */
    private $_postData;
    
    /**
     * Stores the instance of service authentication
     * 
     * @var Core_Service_Authentication
     */
    private $_authentication;
    
    /**
     * Stores the login form
     * 
     * @var Infrastructure_Form_Abstract
     */
    private $_form;
    
    /**
     * Stores the instance of token
     * 
     * @var Core_Service_Token
     */
    private $_token;
    
    /**
     * Stores the result of authentication
     * 
     * @var boolean
     */
    protected $_authenticationResult = false;
    
    /**
     * Stores the generated token value
     * 
     * @var int
     */
    private $_tokenValue;
    
    public function init()
    {
        $this->_postData = $this->getController()->getRequest()->getPost();
        $this->_authentication = new Core_Service_Authentication();
        $this->_token = new Core_Service_Token();
        $this->_form = $this->_getForm();
    }
    
    public function makeAction()
    {
        $this->_authenticationResult = $this->_authorizeUser();
        $this->_prepareHeader();
        $this->_prepareToRender();
        $this->_prepareRequestParams();

        return $this->_authenticationResult;
    }
    
    public function getResponse()
    {
        return $this->_response;
    }
    
    /**
     * Prepares the view to render
     */
    protected function _prepareToRender($messages = null)
    {
        $response = array('status' => null, 'token' => null, 'errors' => null);
        if ($this->_authenticationResult) {
            $response['status'] = 200;
            $response['token'] = $this->_token->get();
        } else if ($this->_form->isValid($this->_postData)) {
            $response['status'] = 404;
        } else {
            $response['status'] = 406;
            $response['errors'] = $this->_form->getErrors();
        }
        
        $this->_response = json_encode($response);
    }
    
    /**
     * Prepares the content of headers
     * 
     * @return void
     */
    protected function _prepareHeader()
    {
        if ($this->_authenticationResult) {
            $this->getController()->getResponse()->setHttpResponseCode(200);
        } else if ($this->_form->isValid($this->_postData)) {
            $this->getController()->getResponse()->setHttpResponseCode(404);
        } else {
            $this->getController()->getResponse()->setHttpResponseCode(406);
        }
    }
    
    /**
     * Prepares the content of session
     * 
     * @return void
     */
    protected function _prepareSession()
    {
        if ($this->_authenticationResult) {
            $session = Zend_Session::namespaceGet('Application');
            if ($session->isLocked()) {
                $session->unlock();
            }

            $session->token = $this->_getToken();
            Zend_Session::regenerateId();
            $session->lock();
        }
    }
    
    /**
     * Prepares the content of request params
     * 
     * @return void
     */
    protected function _prepareRequestParams()
    {
        if ($this->_authenticationResult) {
            $this->getController()->getRequest()
                ->setParam('token', $this->_getToken());
        }
    }
    
    /**
     * Returns the instance of form
     * 
     * @return Infrastructure_Form_Interface
     */
    abstract protected function _getForm();
    
    /**
     * Executes authentication process through data of post
     * 
     * @return boolean
     */
    private function _authorizeUser()
    {
        if ($this->_form->isValid($this->_postData)) {
            return $this->_authentication->authenticate(
                array(
                    'email'=>$this->_postData['email'],
                    'password'=>$this->_postData['password']
                )
            );
        }
        
        return false;
    }
    
    /**
     * Returns the value of token
     * 
     * @return int
     */
    private function _getToken()
    {
        if (empty($this->_tokenValue)) {
            $this->_tokenValue = $this->_token->get();
        }
        
        return $this->_tokenValue;
    }
}
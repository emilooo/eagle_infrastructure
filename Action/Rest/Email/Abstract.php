<?php
/**
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for rest controllers which sending email
 * messages
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Email_Abstract
extends Infrastructure_Action_Rest_Abstract
{
    /**
     * Stores the instance of email service
     * 
     * @var Infrastructure_Service_Email_Interface
     */
    protected $_emailService;
    
    /**
     * Stores the data from json response
     * 
     * @var stdClass
     */
    protected $_jsonResponse;
    
    /**
     * Stores the information about result of executing action
     * 
     * @var boolean
     */
    protected $_operationResult;
    
    public function __construct(
        \Infrastructure_Controller_Rest_Email_Abstract $controller,
        $options = array()
    )
    {
        parent::__construct($controller, $options);
    }
    
    public function init()
    {
        $this->_emailService = $this->_makeEmailService();
        $this->_jsonResponse = json_decode(
            $this->getController()->getRequest()->getRawBody()
        );
    }
    
    /**
     * Executes the process of sending e-mail message
     * 
     * @return boolean
     */
    public function makeAction()
    {
        assert(
            $this->_emailService
            instanceof Infrastructure_Service_Email_Interface
        );
        
        if (!empty($this->_jsonResponse)
            || $this->_jsonResponse instanceof stdClass) {
            $this->_operationResult = $this->_emailService->sendMessage(
                $this->_jsonResponse->sender,
                $this->_jsonResponse->subject,
                $this->_jsonResponse->content
            );
            
            return $this->_operationResult;
        } else {
            throw new Exception("Invalid data to send!");
        }
    }
    
    /**
     * Returns the respond by json format
     * - message string Information about status of sending e-mail message for humans
     * - code int Information about status of sending email as a code of http request
     * - errors array Information about errors
     * 
     * @return json 
     */
    public function getResponse()
    {
        if ($this->_operationResult) {
            $response = array(
                'message' => 'Message has sended!',
                'code' => 201
            );
        } else {
            $response = array(
                'message' => 'Message hasn\'t sended!',
                'code' => 406,
                'errors' => $this->_emailService->getErrors()
            );
        }
        
        return $this->_prepareToRender($response);
    }
    
    protected function _prepareToRender($messages = null) {
        return json_encode($messages);
    }
    
    /**
     * Returns the instance of e-mail service
     * 
     * @return Infrastructure_Service_Email_Interface
     */
    abstract protected function _makeEmailService();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for actions to error handling
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Error_Abstract
extends Infrastructure_Action_Rest_Abstract
{
    /**
     * Returns the code number for response
     * 
     * @return int
     */
    abstract protected function _getResponseCode();
    
    /**
     * Returns the response message
     * 
     * @return string
     */
    abstract protected function _getResponseMessage();
    
    /**
     * Returns the content of response
     * 
     * @return array
     */
    public function getResponse()
    {
        $exception = $this->getController()->getException();
        $response = array(
            'code' => $this->_getResponseCode(),
            'message' => $this->_getResponseMessage()
        );
        if ($exception instanceof Exception
            && APPLICATION_ENV == 'testing') {
            $response['exception']['message'] = $exception->getMessage();
            $response['exception']['file'] = $exception->getFile();
            $response['exception']['line'] = $exception->getLine();
        }
        
        return $response;
    }
    
    /**
     * Preparing the response message to json format
     * 
     * @param array $messages
     */
    protected function _prepareToRender($messages = null)
    {
        assert(isset($messages['code']));
        assert(isset($messages['message']));
        
        $messageToJson = json_encode($messages);
        $this->getController()->getResponse()
            ->setHttpResponseCode($messages['code']);
        $this->getController()->getResponse()->setBody($messageToJson);
    }
    
    /**
     * @return boolean
     */
    public function makeAction()
    {
        $response = $this->getResponse();
        $this->_prepareToRender($response);
        
        return true;
    }
}
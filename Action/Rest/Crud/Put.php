<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for action of put to use controller
 * of rest with crud
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Crud_Put
extends Infrastructure_Action_Rest_Crud_Abstract
{
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * Stores the data to create
     * 
     * @var array
     */
    protected $_data;
    
    /**
     * Stores the result of creating item
     * 
     * @var int
     */
    protected $_result;
    
    public function init()
    {
        $this->_model = $this->getController()->getModel();
    }
    
    /**
     * Returns does action is ready or not
     * 
     * @return boolean
     */
    public function makeAction()
    {
        $this->_result = $this->_updateRecord();
        $this->_prepareToRender();
        if ($this->_result) {
            return true;
        }
        
        return false;
    }
    
    protected function _prepareToRender($messages = null)
    {
        $getUpdateForm = $this->_model->updateForm;
        $getFormErrors = (!$this->_result)
            ? $this->_model->getForm($getUpdateForm)->getErrors() : array();
        $status = ($this->_result) ? 201 : 406;
        $response = array(
            'status' => $status,
            'errors' => $getFormErrors,
            'exceptions' => array()
        );
        
        $this->_response = json_encode($response);
    }
    
    /**
     * Returns the data to update
     * 
     * @return array
     */
    abstract protected function _getData();
    
    /**
     * Returns the number of id to update
     * 
     * @return int
     */
    abstract protected function _getId();
    
    /**
     * Returns result of updating row
     * 
     * @return int Number of id of row
     */
    abstract protected function _updateRecord();
}
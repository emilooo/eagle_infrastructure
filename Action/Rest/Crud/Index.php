<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for action of index to use controller
 * of rest with crud
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Crud_Index
extends Infrastructure_Action_Rest_Crud_Abstract
{
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * Stores the instance of row
     * 
     * @var Zend_Db_Table_Rowset_Abstract
     */
    protected $_records;
    
    public function init()
    {
        $this->_model = $this->getController()->getModel();
    }
    
    /**
     * Returns does action is ready or not
     * 
     * @return boolean
     */
    public function makeAction()
    {
        $this->_records = $this->_getRecords();
        $this->_prepareToRender();
        if ($this->_records->count() > 0) {
            return true;
        }
        
        return false;
    }
    
    protected function _prepareToRender($messages = null)
    {
        $getRecords = $this->_records;
        $convertedRecords = array();
        $rowsetToArray = $this->_convertRowsetToArray($getRecords);
        foreach ($rowsetToArray as $recordId => $recordValue) {
            $convertedRecords[$recordId]
                = $this->_model->toArrayWithoutColumnName($recordValue);
        }
        $status = ($this->_records->count() > 0) ? 200 : 400;
        $errors = ($this->_records->count() > 0)
            ? array() : array('records' => 'Items not exist');
        $response = array(
            'status' => $status,
            'errors' => $errors,
            'exceptions' => array(),
            'records' => $convertedRecords
        );
        $this->_response = json_encode($response);
    }
    
    /**
     * Returns the number of page
     * 
     * @return int
     */
    abstract protected function _getPage();
    
    /**
     * Returns the instance of row
     * 
     * @return Zend_Db_Table_Rowset_Abstract
     */
    abstract protected function _getRecords();
    
    /**
     * Converts rowset to array
     * 
     * @param Zend_Paginator|Zend_Db_Table_Rowset_Abstract $rowset
     * @return array
     */
    private function _convertRowsetToArray($rowset)
    {
        if ($rowset instanceof Zend_Paginator) {
            $convertedData = array();
            foreach ($rowset as $rowIndex => $rowContent) {
                $convertedData[$rowIndex] = $rowContent;
            }
            
            return $convertedData;
                    
        } else if ($rowset instanceof Zend_Db_Table_Rowset_Abstract) {
            return $rowset->toArray();
        }
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for action of get to use controller of rest
 * with crud
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Crud_Get
extends Infrastructure_Action_Rest_Crud_Abstract
{
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * Stores the number of item
     * 
     * @var int
     */
    protected $_id;
    
    /**
     * Stores the instance of row
     * 
     * @var Zend_Db_Table_Row_Abstract
     */
    protected $_record;
    
    public function init()
    {
        $this->_model = $this->getController()->getModel();
        $this->_view = $this->getController()->view;
        $this->_id = (int) $this->_getId();
    }
    
    /**
     * Returns does action is ready or not
     * 
     * @return boolean
     */
    public function makeAction()
    {
        $this->_record = $this->_getRecord();
        $this->_prepareToRender();
        if (!empty($this->_record)) {
            return true;
        }
        
        return false;
    }
    
    protected function _prepareToRender($messages = null)
    {
        $rowToArrayResult = $this->_model->toArrayWithoutColumnName(
            $this->_record->toArray()
        );
        $status = (!empty($this->_record)) ? 200 : 404;
        $errors = (empty($this->_record))
            ? array('record' => 'Item not exist') : array();
        $response = array(
            'status' => $status,
            'errors' => $errors,
            'exceptions' => array(),
            'record' => $rowToArrayResult,
        );
        $this->_response = json_encode($response);
    }
    
    /**
     * Returns the number of record id
     * 
     * @return int
     */
    abstract protected function _getId();
    
    /**
     * Returns the instance of row
     * 
     * @return Zend_Db_Table_Row_Abstract
     */
    abstract protected function _getRecord();
}
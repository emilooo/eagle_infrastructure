<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for action of delete to use controller
 * of rest with crud
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Crud_Delete
extends Infrastructure_Action_Rest_Crud_Abstract
{
    /**
     * Stores the instance of crud model
     * 
     * @var Infrastructure_Model_Crud_Interface
     */
    protected $_model;
    
    /**
     * Stores the result of deleting record
     * 
     * @var int
     */
    protected $_result;
    
    public function init()
    {
        $this->_model = $this->getController()->getModel();
    }
    
    public function makeAction()
    {
        $this->_result = $this->_deleteRecord();
        $this->_prepareToRender();
        if ($this->_result > 0) {
            return true;
        }
        
        return false;
    }
    
    protected function _prepareToRender($messages = null)
    {
        $status = ($this->_result > 0) ? 200 : 404;
        $response = array(
            'status' => $status,
            'exceptions' => array()
        );
        $this->_response = json_encode($response);
    }
    
    /**
     * Returns the number for deleting the record
     * 
     * @return int
     */
    abstract protected function _getId();
    
    /**
     * Executes the delete record
     * 
     * @return int
     */
    abstract protected function _deleteRecord();
}
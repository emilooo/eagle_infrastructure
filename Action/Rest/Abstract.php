<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Action
 * @subpackage Rest
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for actions that using controller of rest
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Action
 * @subpackage Rest
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Action_Rest_Abstract
extends Infrastructure_Action_Abstract
{
    /**
     * Stores the response in the format of json
     * 
     * @var string
     */
    protected $_response;
    
    public function __construct(
    \Infrastructure_Controller_Rest_Interface $controller,
        $options = array()
    )
    {
        parent::__construct($controller, $options);
    }
    
    /**
     * Returns the response in the format of json
     * 
     * @return string
     */
    abstract public function getResponse();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Action
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Exception for hanling not exist action
 * 
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Action
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Action_NotExist
extends Infrastructure_Exception_Base
{
}
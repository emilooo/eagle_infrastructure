<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Form
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Makes the insatnce of form for class
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Form
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Form_Factory
{
    /**
     * Makes the instance of form through
     * name of class and name of form
     * 
     * @param string $className Full name of class for which generates
     * the instance of form
     * @param string $formName Short name of form
     */
    public static function make($className, $formName)
    {
        $getResourceClassNameResult
            = self::_getResourceClassName($className, $formName);
        
        try{
            $formClassInstance = new $getResourceClassNameResult();
            
            return $formClassInstance;
        } catch (Exception $exceptions) {
            throw new Infrastructure_Model_InvalidOperation(
                'Form class ' . $getResourceClassNameResult
                . ' cannot be loaded. ' . $exceptions->getMessage()
            );
        }
    }
    
    /**
     * Rteurns the full class name of form
     * 
     * @param string $className Full name of class
     * @param string $formName Short name of form
     * @return string Full name of form
     * @throws Exception
     */
    private static function _getResourceClassName($className, $formName)
    {
        $isCorrectClassName = is_string($className);
            assert($isCorrectClassName, 'Parameter: className is bad!');
        $isCorrectFormName = is_string($formName);
            assert($isCorrectFormName, 'Parameter: formName is bad!');
        $explodeCurrentClassNameResult = explode('_', $className);
        $libraryName = $explodeCurrentClassNameResult[0];
        $formClassName
            = $libraryName .'_Form_' . $explodeCurrentClassNameResult[2]
            . '_' . $formName;
        
        return $formClassName;
    }
}


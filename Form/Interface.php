<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Form
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides interface for all forms in application
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Form
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Form_Interface
{
}


<?php
/**
 * Copyright (C) 2015 emilooo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category Infrastructure
 * @package Infrastructure_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for form fields
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Form_Field_Abstract
implements Infrastructure_Form_Field_Interface
{
    /**
     * Stores the instance of form
     * 
     * @var Infrasttructure_Form_Interface
     */
    protected $_form;
    
    public function __construct(
        Infrastructure_Form_Interface $form,
        $options = array()
    )
    {
        $this->_form = $form;
        $this->init();
        $this->setProperties($options);
    }
    
    /**
     * Initiates the configuration of class
     */
    public function init()
    {
    }
    
    
    public function getForm()
    {
        return $this->_form;
    }
    
    /**
     * Sets the properties of class
     * 
     * @throws Exception
     * @param array $options
     * @return void
     */
    public function setProperties($options = array())
    {
        assert(is_array($options), 'Property options is not array!');
        
        foreach ($options as $propertyName => $propertyValue) {
            $propertyName = '_' . $propertyName;
            if (property_exists($this, $propertyName)) {
                $this->$propertyName = $propertyValue;
            }
        }
    }
}
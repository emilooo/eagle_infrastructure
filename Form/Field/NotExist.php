<?php
/**
 * Copyright (C) 2015 emilooo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category Infrastructure
 * @package Infrastructure_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Not exist form field exception
 * 
 * @category Infrastructure
 * @package Infrastructure_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Form_Field_NotExist
extends Infrastructure_Exception_Base
{
}
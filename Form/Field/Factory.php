<?php
/**
 * Copyright (C) 2015 emilooo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category Infrastructure
 * @package Infrastructure_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Makes the instance of form field
 * 
 * @category Infrastructure
 * @package Infrastructure_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Form_Field_Factory
extends Infrastructure_Factory
{
    private static $_instance;
    
    public static function getInstance()
    {
        if (!is_object(self::$_instance)) {
            $getCurrentClass = get_called_class();
            self::$_instance = new $getCurrentClass();
        }
        
        return self::$_instance;
    }
    
    public function make($resourceName, $parentClass, $options = array())
    {
        $this->_setResourceName($resourceName);
        $this->_setParentClass(get_class($parentClass));
        
        try {
            $getClassNameResult = $this->_getResourceClassName();
            $classInstance = new $getClassNameResult($parentClass);
            
            return $classInstance;
        } catch (Exception $ex) {
            throw $this->_getException($ex);
        }
    }
    
    protected function _getResourceClassName()
    {
        $getParentClassNameResult = $this->_getParentClass();
        $getResourceNameResult = $this->_getResourceName();
        
        $sliceParentClass = explode('_', $getParentClassNameResult);
        $moduleName = $sliceParentClass[0];
        $className = $moduleName. '_' . 'Form_' . $sliceParentClass[2] . '_Field_'
        . $getResourceNameResult;
        
        return $className;
    }
    
    protected function _getException()
    {
        $getResourceNameResult = $this->_getResourceName();
        
        return new Infrastructure_Form_Field_NotExist(
            'Form field ' . $getResourceNameResult . ' not exist! '
        );
    }
}
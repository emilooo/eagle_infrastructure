<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Form
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for implementation of form elements
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Form
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Form_Element_Interface
{
    /**
     * Returns instance of form element
     * 
     * @return Zend_Form_Element
     */
    public function getElement();
}
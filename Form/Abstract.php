<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Form
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides primary implementation for all forms in application
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Form
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Form_Abstract
extends Zend_Form
implements Infrastructure_Form_Interface
{
    /**
     * Instance of application model
     * 
     * @var Infrastructure_Model_Interface
     */
    protected $_model;
    
    /**
     * Stores the form fields
     * 
     * @var array
     */
    protected $_fields = array();
    
    /**
     * Stores the instance of form field factory
     * 
     * @var Infrastructure_Form_Field_Factory
     */
    protected $_factory;
    
    public function __construct($options = array())
    {
        parent::__construct($options);
        $this->setProperties($options);
    }
    
    public function init()
    {
        parent::init();
    }
    
    /**
     * Seting application model instance
     * 
     * @param Infrastructure_Model_Interface $modelInstance Instance
     * of application model
     * @return void
     */
    public function setModel(
        Infrastructure_Model_Interface $modelInstance
    )
    {
        $this->_model = $modelInstance;
    }
    
    /**
     * Returns instance of application model
     * 
     * @return Infrastructure_Model_Interface
     */
    public function getModel()
    {
        return $this->_model;
    }
    
    /**
     * Returns the created instance of form field and saves
     * this instance in the list
     * 
     * @param string $fieldName
     * @param array $options
     * @return Infrastructure_Form_Field_Interface
     */
    public function getField($fieldName, $options = array())
    {
        if (!array_key_exists($fieldName, $this->_fields)) {
            $getFieldFactory = Infrastructure_Form_Field_Factory::getInstance();
            $makeResult = $getFieldFactory->make(
                $fieldName, $this, $options
            );
            $this->_fields[$fieldName] = $makeResult;
        }
        
        return $this->_fields[$fieldName];
    }
    
    /**
     * Sets the properties of class
     * 
     * @throws Exception
     * @param array $options
     * @return void
     */
    public function setProperties($options = array())
    {
        assert(is_array($options), 'Property options is not array!');
        
        foreach ($options as $propertyName => $propertyValue) {
            $propertyName = '_' . $propertyName;
            if (property_exists($this, $propertyName)) {
                $this->$propertyName = $propertyValue;
            }
        }
        
        return $this;
    }
}
<?php
/**
 * @category Infrastructure
 * @package Infrastructure_Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the bacic implementation for all services of application
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Service_Abstract
{
    /**
     * Initiates the settings of class
     * 
     * @return void
     */
    abstract public function init();
    
    /**
     * Constructor of class that initiates the settings of class and sets
     * the properties of class
     * 
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->init();
        $this->setProperties($options);
    }
    
    /**
     * Sets property of class
     * 
     * @param array $options
     * @return void
     */
    public function setProperties($options)
    {
        assert(is_array($options));
        
        if (!empty($options)) {
            foreach ($options as $propertyKey=>$propertyValue) {
                $propertyName = '_'.$propertyKey;
                if (isset($propertyName)) {
                    $this->$propertyName = $propertyValue;
                }
            }
        }
    }
}
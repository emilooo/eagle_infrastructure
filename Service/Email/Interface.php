<?php

interface Infrastructure_Service_Email_Interface
{
    /**
     * Sending the e-mail message
     * 
     * @param string $sender E-mail address
     * @param string $subject Subject of message
     * @param string $content Content of message
     * @return boolean
     */
    public function sendMessage($sender, $subject, $content);
    
    /**
     * Returns the errors if exists
     * 
     * @return array
     */
    public function getErrors();
}
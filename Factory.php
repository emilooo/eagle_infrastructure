<?php
/**
 * Copyright (C) 2015 emilooo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Factory
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic interface for all factories which exist
 * in the application.
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Factory
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Factory
{
    /**
     * Stores the resource name
     * 
     * @var string
     */
    private $_resourceName;
    
    /**
     * Stores the name of parent class
     * 
     * @var string
     */
    private $_parentClass;
    
    /**
     * Returns the instance of self class
     * 
     * @static
     * @return Infrastructure_Factory
     */
    public static function getInstance()
    {
    }
    
    /**
     * Sets the name of resource
     * 
     * @param string $resourceName
     * @throws Exception
     * @return \Infrastructure_Factory
     */
    protected function _setResourceName($resourceName)
    {
        $isCorrectResourceName = is_string($resourceName);
            assert($isCorrectResourceName);
        $this->_resourceName = $resourceName;
        return $this;
    }
    
    /**
     * Sets the name of parent class
     * 
     * @param string $parentClass Name of parent class
     * @throws Exception
     * @return \Infrastructure_Factory
     */
    protected function _setParentClass($parentClass)
    {
        $isCorrectParentClass = is_string($parentClass);
            assert($isCorrectParentClass);
        $this->_parentClass = $parentClass;
        return $this;
    }
    
    /**
     * Returns the name of resource
     * 
     * @return string
     */
    protected function _getResourceName()
    {
        return $this->_resourceName;
    }
    
    /**
     * Returns the name of parent class
     * 
     * @return string
     */
    protected function _getParentClass()
    {
        return $this->_parentClass;
    }
    
    /**
     * Makes the instance of resource.
     * 
     * @param string $resourceName Name of resource
     * @param object $parentClas Instance of parent class
     * @param array $options Array of options
     */
    abstract public function make(
        $resourceName, $parentClass, $options = array()
    );
    
    /**
     * Returns the name of resource class
     * 
     * @return string
     */
    abstract protected function _getResourceClassName();
    
    /**
     * Returns the instance of exception for handling
     * the behavior when resource class name not exist
     * 
     * @return Exception
     */
    abstract protected function _getException();
    
    /**
     * Disable constructor of class
     */
    protected function __construct()
    {
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for interface of auth
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Model_Auth_Abstract
extends Infrastructure_Model_Crud_Abstract
{
    /**
     * Stores the name of register form
     * 
     * @var string
     */
    public $registerForm = 'Register';
    
    /**
     * Stores the name of remind form
     * 
     * @var string
     */
    public $remindForm = 'Remind';
    
    /**
     * Registers the new user
     * 
     * @param array $dataToRegister
     * @return boolean
     */
    public function registerItem($dataToRegister)
    {
        $getCreateFormResult = $this->getForm($this->registerForm);
        if ($getCreateFormResult->isValid($dataToRegister)) {
            $transformColumnsResult
                = $this->_transformColumns($getCreateFormResult->getValues());
            return $this->getResource($this->getResourceId())
                ->createItem($transformColumnsResult);
        }
        
        return false;
    }
}
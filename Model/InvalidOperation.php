<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Invalid operation exception
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Model_InvalidOperation
extends Infrastructure_Exception_Base
{
}
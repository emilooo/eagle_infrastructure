<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Cache
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic implementation for handling caching of model
 * instance that uses with database
 * 
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Cache
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Model_Cache
extends Infrastructure_Model_Cache_Abstract
{
    public function init()
    {
        parent::init();
        
        $configuration = $this->_getCacheConfiguration();
        $this->setBackend(
            $configuration['backend']['name'],
            $configuration['backend']['options']
        );
        $this->setFrontend(
            $configuration['frontend']['name'],
            $configuration['frontend']['options']
        );
    }
    
    public function setCache(Zend_Cache $cache)
    {
        $this->_cache = $cache;
        
        return $this;
    }
    
    public function getCache()
    {
        if (null === $this->_cache) {
            $this->_frontendOptions['cached_entity'] = $this->_model;
            $this->_cache = Zend_Cache::factory(
                $this->_frontendName,
                $this->_backendName,
                $this->_frontendOptions,
                $this->_backendOptions
            );
        }
        
        return $this->_cache;
    }
    
    public function setBackend($backendName, $options = array())
    {
        $isCorrectBackend = is_string($backendName);
            assert($isCorrectBackend);
        $isCorrectOptions = is_array($options);
            assert($isCorrectOptions);
        
        $this->_backendName = $backendName;
        $this->_backendOptions = $options;
        
        return $this;
    }
    
    public function setFrontend($frontendName, $options = array())
    {
        $isCorrectFrontend = is_string($frontendName);
            assert($isCorrectFrontend);
        $isCorrectOptions = is_array($options);
            assert($isCorrectOptions);
            
        $this->_frontendName = $frontendName;
        $this->_frontendOptions = $options;
        $this->_frontendOptions['cached_entity'] = $this->_model;
        
        return $this;
    }
    
    public function setTagged($tagName = null)
    {
        $isCorrectTagName = (is_string($tagName) || is_null($tagName));
            assert($isCorrectTagName);
        
        $this->_tagged = (empty($tagName)) ? 'default' : $tagName;
        
        return $this;
    }
    
    protected function _getCacheConfiguration()
    {
        $getConfiguration = Zend_Registry::get('config');
        $cacheConfiguration = $getConfiguration['resources']['cachemanager']
            ['database']['model'];
        
        return $cacheConfiguration;
    }
}
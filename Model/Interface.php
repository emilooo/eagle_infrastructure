<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides interface for all models of application
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Model_Interface
{
    /**
     * Returns instance of model resource
     * 
     * @param string $resourceName Name of model
     * @return Infrastructure_Resource
     */
    public function getResource($resourceName);
    
    /**
     * Returns instance of form in application
     * 
     * @param string $formName Name of form
     * @return Infrastructure_Form
     */
    public function getForm($formName);
}
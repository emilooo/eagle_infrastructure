<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Cache
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for handling caching of model
 * instance that uses with database
 * 
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Cache
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Model_Cache_Abstract
{
    /**
     * Stores the instance of model
     * 
     * @var Infrastructure_Model_Abstract
     */
    protected $_model;
    
    /**
     * Stores the instance of cache
     * 
     * @var Zend_Cache
     */
    protected $_cache = null;
    
    /**
     * Stores the type of frontend cache, should be Class
     * 
     * @var string
     */
    protected $_frontendName;
    
    /**
     * Stores the type of backend cache
     * 
     * @var string
     */
    protected $_backendName;
    
    /**
     * Stores the options of frontend
     * 
     * @var array
     */
    protected $_frontendOptions = array();
    
    /**
     * Stores the options of backend
     * 
     * @var array
     */
    protected $_backendOptions = array();
    
    /**
     * Stores the tag this call will be stored against
     * 
     * @var string
     */
    protected $_tagged;
    
    /**
     * @param Infrastructure_Model_Abstract $model
     * @param array $options
     * @param string $tagged
     */
    public function __construct(
        Infrastructure_Model_Abstract $model,
        $options = array(),
        $tagged = null
    )
    {
        $this->_model = $model;
        $this->setTagged($tagged);
        $this->init();
        $this->setOptions($options);
    }
    
    public function init()
    {
    }
    
    /**
     * Sets the instance of cache
     * 
     * @abstract
     * @param Zend_Cache $cache
     * @return \Infrastructure_Model_Cache_Abstract
     */
    abstract public function setCache(Zend_Cache $cache);
    
    /**
     * Returns the instance of cache
     * 
     * @abstract
     * @return Zend_Cache
     */
    abstract public function getCache();
    
    /**
     * Sets the type of backend property and sets the options
     * 
     * @abstract
     * @param string $backendName
     * @param array $options
     */
    abstract public function setBackend($backendName, $options = array());
    
    /**
     * Sets the value of frontend property
     * 
     * @abstract
     * @param string $frontendName
     * @param array $options
     */
    abstract public function setFrontend($frontendName, $options = array());
    
    /**
     * Sets the name of tag to cached resource
     * 
     * @abstract
     * @param string $tagName
     * @return \Infrastructure_Model_Cache_Abstract
     */
    abstract public function setTagged($tagName = null);
    
    public function setOptions($options)
    {
        $isCorrectOptions = is_array($options);
            assert($isCorrectOptions);
        
        foreach ($options as $optionIndex => $optionValue) {
            $propertyName = '_' . $optionIndex;
            if (property_exists($this, $propertyName)) {
                $this->$propertyName = $optionValue;
            }
        }
        
        return $this;
    }
    
    /**
     * Proxy. Redirect request to methods of model
     * 
     * @param string $methodName
     * @param mixed $methodArguments
     * @return callable
     * @throws Infrastructure_Model_InvalidOperation
     */
    public function __call($methodName, $methodArguments)
    {
        if (!is_callable(array($this->_model, $methodName))) {
            throw new Infrastructure_Model_InvalidOperation(
                'Method ' . $methodName . ' does not exist in class '
                . get_class($this->_model)
            );
        }
        
        $cache = $this->getCache();
        $cache->setTagsArray(array($this->_tagged));
        $callback = array($cache, $methodName);
        
        return call_user_func_array($callback, $methodArguments);
    }
    
    /**
     * Returns the cache configuration
     * 
     * @return array
     */
    abstract protected function _getCacheConfiguration();
}
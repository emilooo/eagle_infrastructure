<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Resource
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Makes the insatnce of resource model for class
 * 
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Resource
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Model_Resource_Factory
{
    /**
     * Makes the instance of resource model through
     * name of class and name of resource
     * 
     * @param string $className Full name of class for which generates
     * the instance of resource
     * @param string $resourceName Short name of resource
     */
    public static function make($className, $resourceName)
    {
        $getResourceClassNameResult
            = self::_getResourceClassName($className, $resourceName);
        
        try{
            $resourceClassInstance = new $getResourceClassNameResult();
            
            return $resourceClassInstance;
        } catch (Exception $exceptions) {
            throw new Infrastructure_Model_InvalidOperation(
                'Resource model class ' . $getResourceClassNameResult 
                . ' cannot be loaded.' . ' ' . $exceptions->getMessage()
            );
        }
    }
    
    /**
     * Rteurns the full class name of resource model
     * 
     * @param string $className Full name of class
     * @param string $resourceName Short name of resource
     * @return string Full name of resource of model
     * @throws Exception
     */
    private static function _getResourceClassName($className, $resourceName)
    {
        $isCorrectClassName = is_string($className);
            assert($isCorrectClassName, 'Parameter: className is bad!');
        $isCorrectResourceName = is_string($resourceName);
            assert($isCorrectResourceName, 'Parameter: resourceName is bad!');
        $explodeCurrentClassNameResult = explode('_', $className);
        $libraryName = $explodeCurrentClassNameResult[0];
        $resourceClassName
            = $libraryName .'_Model_Resource_' .$resourceName;
        
        return $resourceClassName;
    }
}
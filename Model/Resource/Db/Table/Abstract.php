<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model_Resource_Db
 * @subpackage Table
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for table of model
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Model_Resource_Db
 * @subpackage Table
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Model_Resource_Db_Table_Abstract
extends Zend_Db_Table_Abstract
{
    /**
     * Saves the only those data which columns exist in the source of data
     * 
     * @param array $dataToSave
     * @param Zend_Db_Table_Row $currentRow
     * @return int Number of saved items
     */
    public function saveItem($dataToSave, $currentRow = null)
    {
        $this->_checkCurrentRow($currentRow);
        $this->_checkDataToSave($dataToSave);
        
        $currentRow = ($currentRow === null) ? $this->createRow() : $currentRow;
        $getColumnsFromTableResult = $this->info('cols');
        foreach ($getColumnsFromTableResult as $column) {
            if (array_key_exists($column, $dataToSave)) {
                $currentRow->$column = $dataToSave[$column];
            }
        }
        
        return $currentRow->save();
    }
    
    /**
     * Checks first parameter in saveRow method
     * 
     * @param array $dataToSave
     */
    private function _checkDataToSave($dataToSave)
    {
        assert(
            (is_array($dataToSave) && !empty($dataToSave)),
            'Bad type of parameter: dataToSave'
        );
    }
    
    private function _checkCurrentRow($currentRow)
    {
        assert(
            ($currentRow instanceof
                Zend_Db_Table_Row_Abstract
                || is_null($currentRow)
            ),
            'Bad type of parameter: currentRow'
        );
    }
}
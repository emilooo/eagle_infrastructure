<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model_Resource_Db_Table
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for handle the operations of CRUD
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Model_Resource_Db_Table
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Model_Resource_Db_Table_Crud_Abstract
extends Infrastructure_Model_Resource_Db_Table_Abstract
implements Infrastructure_Model_Resource_Db_Table_Crud_Interface
{
    public function createItem($dataToSave)
    {
        $isCorrectDataToSave = (is_array($dataToSave) && !empty($dataToSave));
            assert($isCorrectDataToSave);
        
        $saveRowResult = $this->saveItem($dataToSave);
        
        return $saveRowResult;
    }
    
    public function readOneItem($columnName, $columnValue)
    {
        $isCorrectColumnName = is_string($columnName);
            assert($isCorrectColumnName);
        $isColumnExist = array_search($columnName, $this->info('cols'));
        $hasTableColumn = !is_bool($isColumnExist);
            assert($hasTableColumn);
        $selectQueryResult
            = $this->select()->where($columnName . " = ?", $columnValue);
        $fetchRowResult = $this->fetchRow($selectQueryResult);
            if (!empty($fetchRowResult)) {
                return $fetchRowResult;
            }
        
        $this->_getExceptionForNotExistItem();
    }
    
    public function readManyItems($pageNumber = null)
    {
        $this->_isCorrectPageNumber($pageNumber);
        
        $selectQueryResult = $this->select();
        if (!empty($pageNumber)) {
            $paginatorAdapter
                = new Zend_Paginator_Adapter_DbSelect($selectQueryResult);
            $paginator = new Zend_Paginator($paginatorAdapter);
            return $paginator->setCurrentPageNumber($pageNumber)
                ->setItemCountPerPage(10);
        }
        
        return $this->fetchAll($selectQueryResult);
    }
    
    public function updateItems($columnName, $columnValue, $dataToUpdate)
    {
        $readOneItemResult = $this->readOneItem($columnName, $columnValue);
        if (!empty($readOneItemResult)) {
            return $this->saveItem($dataToUpdate, $readOneItemResult);
        }
        
        $this->_getExceptionForNotExistItem();
    }
    
    public function deleteItems($columnName, $columnValue)
    {
        $readOneItemResult = $this->readOneItem($columnName, $columnValue);
        if (!empty($readOneItemResult)) {
            
            return $this->getAdapter()->delete(
                $this->_name,
                array($columnName . ' = ?'
                    => $this->_db->quote($columnValue)
                )
            );
        }
        
        $this->_getExceptionForNotExistItem();
    }
    
    /**
     * Throws the exception for not exist item
     * 
     * @throws Infrastructure_Model_Resource_Db_Table_Row_NotExist
     */
    private function _getExceptionForNotExistItem()
    {
        throw new Infrastructure_Model_Resource_Db_Table_Row_NotExist(
            'Item not exist'
        );
    }
    
    private function _isCorrectPageNumber($pageNumber)
    {
        $isCorrectPageNumber = (
            is_int($pageNumber) || is_numeric($pageNumber)
                || is_null($pageNumber)
        );
        assert($isCorrectPageNumber);
        
        return true;
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model_Resource_Db_Table
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the primary interface for handle the operations of CRUD
 * 
 * @category Infrastructure
 * @package Infrastructure_Model_Resource_Db_Table
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
interface Infrastructure_Model_Resource_Db_Table_Crud_Interface
extends Infrastructure_Model_Resource_Interface
{
    /**
     * Creates the new item
     * 
     * @param array $dataToSave
     * @return int Number of created items
     */
    public function createItem($dataToSave);
    
    /**
     * Retruns the instance of item
     * 
     * @param string $columnName Name of column from database
     * @param mixed $columnValue Value of column
     * @return Zend_Db_Table_Row|null Instance of row
     */
    public function readOneItem($columnName, $columnValue);
    
    /**
     * Returns the many items
     * 
     * @param int|null $pageNumber Number of page
     * @return Zend_Db_Table_Rowset|Zend_Paginator Instance of rowset
     * or paginator
     */
    public function readManyItems($pageNumber = null);
    
    /**
     * Updates the one or many items
     * 
     * @param string $columnName Name of column from database
     * @param mixed $columnValue Value of column
     * @param array $dataToUpdate Values for update
     * @return int Number of updated items
     */
    public function updateItems($columnName, $columnValue, $dataToUpdate);
    
    /**
     * Deletes the one or many items
     * 
     * @param string $columnName Name of column from database
     * @param mixed $columnValue Value of column
     * @return int Number of deleted items
     */
    public function deleteItems($columnName, $columnValue);
}
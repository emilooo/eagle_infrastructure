<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model_Resource_Db_Table
 * @subpackage Row
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the interface for not existing row
 * 
 * @category Infrastructure
 * @package Infrastructure_Model_Resource_Db_Table
 * @subpackage Row
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
class Infrastructure_Model_Resource_Db_Table_Row_NotExist
extends Infrastructure_Exception_Base
{
}
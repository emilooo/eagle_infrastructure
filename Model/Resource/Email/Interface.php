<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Resource_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for table of model
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Resource_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Model_Resource_Email_Interface
{
    /**
     * Sending the email message
     * 
     * @param array $dataToSend Data to send
     * @return boolean Returns true if e-mail has sended else false
     */
    public function sendMessage($dataToSend);
}
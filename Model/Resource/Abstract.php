<?php

abstract class Infrastructure_Model_Resource_Abstract
{
    public function __construct($options = array())
    {
        $this->init();
        $this->setProperties($options);
    }
    
    public function init()
    {
    }
    
    /**
     * Sets property of class
     * 
     * @param array $options
     * @return void
     */
    public function setProperties($options)
    {
        assert(is_array($options));
        
        if (!empty($options)) {
            foreach ($options as $propertyKey=>$propertyValue) {
                $propertyName = '_'.$propertyKey;
                if (isset($propertyName)) {
                    $this->$propertyName = $propertyValue;
                }
            }
        }
    }
}
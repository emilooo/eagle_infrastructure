<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Resource
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides interface for resources of model
 * 
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Resource
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Model_Resource_Interface
{
}
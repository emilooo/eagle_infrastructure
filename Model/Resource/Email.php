<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation to sending e-mail message through
 * tree steps, such as:
 * 
 * step one: create the transport e-mail instance,
 * step two: create the structure of e-mail
 * step tree: send created e-mail
 * step four: return result of sending
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Model_Resource_Email
extends Infrastructure_Model_Resource_Abstract
{
    /**
     * Stores the email configuration settings
     * 
     * @var array
     */
    protected $_configuration = null;
    
    /**
     * Stores the instance which defines how send the message of email
     * 
     * @var Zend_Mail_Transport_Abstract
     */
    protected $_transport = null;
    
    /**
     * Returns the configuration for email transport layer
     * 
     * return array
     */
    abstract protected function _getConfiguration();
    
    /**
     * Makes the mail transport and returns her through instance
     * 
     * @return Zend_Mail_Transport_Abstract
     */
    abstract protected function _makeTransprt();
    
    /**
     * Makes the structure of email message and returns her through instance
     * 
     * @return boolean If mail has sended returns true else false
     */
    abstract protected function _makeEmail($dataToSend);
    
    public function init()
    {
        parent::init();
        
        $this->_configuration = (empty($this->_configuration))
            ? $this->_getConfiguration() : $this->_configuration;
        $this->_transport = (empty($this->_transport))
            ? $this->_makeTransprt() : $this->_transport;
    }
    
    public function sendMessage($dataToSend)
    {
        $this->_isCorrectDataToSend($dataToSend);
        $this->_isCorrectConfiguration($this->_configuration);
        $getMailTransportResult = $this->_transport;
            $this->_isCorrectTransport($getMailTransportResult);
        $makeEmailResult = $this->_makeEmail($dataToSend);
            $this->_isCorrectMail($makeEmailResult);
        
        $sendMailResult = $makeEmailResult->send($getMailTransportResult);
        
        return $this->_isCorrectMail($sendMailResult);
    }
    
    /**
     * Verifies if email configuration is correct
     * 
     * @param array $configuration
     * @return boolean
     * @expectedException Exception
     */
    private function _isCorrectConfiguration($configuration)
    {
        assert(is_array($configuration));
        
        return true;
    }
    
    /**
     * Verifies if instance of email transport is correct
     * 
     * @param Zend_Mail_Transport_Abstract $transport
     * @expectedException Exception
     */
    private function _isCorrectTransport($transport)
    {
        assert($transport instanceof Zend_Mail_Transport_Abstract);
        
        return true;
    }
    
    /**
     * Verifies if instance of mail structure is correct
     * 
     * @param Zend_Mail $mail
     * @expectedException Exception
     */
    private function _isCorrectMail($mail)
    {
        assert($mail instanceof Zend_Mail);
        
        return true;
    }
    
    /**
     * Verifies if input parameter of dataToSend is correct array
     * 
     * @param array $dataToSend
     * @return boolean
     * @expectedException Exception
     */
    private function _isCorrectDataToSend($dataToSend)
    {
        assert(is_array($dataToSend));
        assert(!empty($dataToSend));
        
        return true;
    }
}
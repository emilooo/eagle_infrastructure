<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides primary implementation for all models in application
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Model_Abstract
implements Infrastructure_Model_Interface
{
    /**
     * Forms classes array
     * 
     * @var array
     */
    private $_forms = array();
    
    /**
     * Resource classes array
     * 
     * @var array
     */
    private $_resources = array();
    
    /**
     * Stores the instance of class wich handle the cache
     * 
     * @var Infrastructure_Model_Cache
     */
    private $_cache;
    
    /**
     * Constructor
     * 
     * @param array $properties Properties of class
     * @return void
     */
    public function __construct($properties = array())
    {
        $this->init();
        $this->setProperties($properties);
    }
    
    public function getForm($formName)
    {
        if (!isset($this->_forms[$formName])) {
            $formInstance = Infrastructure_Form_Factory::make(
                get_class($this), $formName
            );
            $this->_forms[$formName] = $formInstance;
        }
        
        return $this->_forms[$formName];
    }
    
    public function getResource($resourceName)
    {
        if (!isset($this->_resources[$resourceName])) {
            $resourceInstance = Infrastructure_Model_Resource_Factory::make(
                get_class($this), $resourceName
            );
            $this->_resources[$resourceName] = $resourceInstance;
        }
        
        return $this->_resources[$resourceName];
    }
    
    /**
     * Returns the cached instance of model. The model will be saved
     * if not exist in the register of cache and his tag will be
     * the name of the model
     * 
     * @param string $modelName
     * @return Infrastructure_Model_Cache
     */
    public function getCached($modelName)
    {
        if (empty($this->_cache)) {
            $this->_cache = new Infrastructure_Model_Cache($this);
        }
        
        $this->_cache->setTagged($modelName);
        return $this->_cache;
    }
    
    /**
     * Cleans cached model
     * 
     * @param string $modelName
     * @return Infrastructure_Model_Abstract
     */
    public function cleanCached($modelName)
    {
        $isCorrectModelName = is_string($modelName);
            assert($isCorrectModelName);
        $this->getCached($modelName)->getCache()
            ->clean(
                Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
                array($modelName)
            );
        
        return $this;
    }
    
    /**
     * Sets property of class
     * 
     * @param array $options
     * @return void
     */
    public function setProperties($options)
    {
        assert(is_array($options));
        
        if (!empty($options)) {
            foreach ($options as $propertyKey=>$propertyValue) {
                $propertyName = '_'.$propertyKey;
                if (isset($propertyName)) {
                    $this->$propertyName = $propertyValue;
                }
            }
        }
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the primary interface for model of infrastructure library
 * 
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Model_Acl_Abstract
extends Infrastructure_Model_Abstract
implements Infrastructure_Model_Acl_Interface, Zend_Acl_Resource_Interface
{
    /**
     * Stores the name of identity column name
     * 
     * @const string
     */
    const IDENTITY_COLUMN_NAME = 'user_role';
    
    /**
     * @var Zend_Acl
     */
    protected $_acl;
    
    /**
     * Stores the instance of identity role
     * 
     * @var Zend_Acl_Role_Interface
     */
    protected $_identityRole;
    
    /**
     * Sets the identity through instance of row model
     * 
     * @param Zend_Db_Table_Row $identity
     * @return Infrastructure_Model_Acl_Abstract
     * @todo When will implements the new version of row model that support
     * the name of table in the column names remove the user_column
     * from content of this method
     */
    public function setIdentity($identity)
    {
        $isCorrectIdentity = ($identity instanceof Zend_Db_Table_Row_Abstract);
            assert($isCorrectIdentity);
        $getIdentityColumnResult = self::IDENTITY_COLUMN_NAME;
        $isCorrectRow = !empty($identity->__get($getIdentityColumnResult));
            assert($isCorrectRow);
        
        $this->_identityRole
            = new Zend_Acl_Role($identity->__get($getIdentityColumnResult));
        
        return $this;
    }
    
    /**
     * Returns identity
     * 
     * @return Zend_Acl_Role_Interface
     */
    public function getIdentity()
    {
        if (empty($this->_identityRole)) {
            $getAuthResult = Zend_Auth::getInstance();
            if (!$getAuthResult->hasIdentity()) {
                return 'Guest';
            }
            
            $this->setIdentity($getAuthResult->getIdentity());
        }
        
        return $this->_identityRole;
    }
    
    /**
     * Checks access by actionName in access control list
     * 
     * @param string $actionName
     * @return boolean
     */
    public function checkAcl($actionName)
    {
        $isCorrectActionName = is_string($actionName);
            assert($isCorrectActionName);
        
        return $this->getAcl()->isAllowed(
            $this->getIdentity(),
            $this,
            $actionName
        );
    }
}
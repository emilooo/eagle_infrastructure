<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for implementation of access controll list
 * in model of infrastructure library
 * 
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Model_Acl_Interface
{
    /**
     * Sets the identity
     * 
     * @param array|string|null|Zend_Acl_Role_Interface $identity
     * @return Infrastructure_Model_Abstract
     */
    public function setIdentity($identity);
    
    /**
     * Returns the identity
     * 
     * @return Zend_Acl_Role_Interface
     */
    public function getIdentity();
    
    /**
     * Checks access to action
     * 
     * @param string $actionName Action name
     */
    public function checkAcl($actionName);
    
    /**
     * Sets the acl instance
     * 
     * @param Infrastructure_Acl_Interface $acl
     */
    public function setAcl(Infrastructure_Acl_Interface $acl);
    
    /**
     * Returns the acl instance
     * 
     * @return Zend_Acl_Role_Interface
     */
    public function getAcl();
}
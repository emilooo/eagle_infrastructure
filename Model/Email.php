<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for email model
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Model_Email
extends Infrastructure_Model_Acl_Abstract
implements Infrastructure_Model_Resource_Email_Interface
{
    /**
     * Returns the instance of e-mail form
     * 
     * @return Infrastructure_Form_Abstract
     */
    abstract protected function _getForm();
    
    public function sendMessage($dataToSend) {
        if (!$this->checkAcl('sendMessage')) {
            $this->_throwAccessDeniedException();
        }
        
        $emailForm = $this->_getForm();
        $this->_isCorrectForm($emailForm);
        if ($emailForm->isValid($dataToSend)) {
            $emailForm->populate($dataToSend);
            return $this->getResource($this->getResourceId())
                ->sendMessage($dataToSend);
        }
        
        return false;
    }
    
    /**
     * Verifies if input parameter of emailForm is correct form instance
     * 
     * @param Infrastructure_Form_Abstract $emailForm
     */
    private function _isCorrectForm($emailForm)
    {
        assert($emailForm instanceof Infrastructure_Form_Abstract);
        
        return true;
    }
    
    /**
     * Throws the execute of not premissions to execute operation
     * 
     * @throws Infrastructure_Acl_AccessDenied
     */
    protected function _throwAccessDeniedException()
    {
        throw new Infrastructure_Acl_AccessDenied(
            'You haven\'t permissions to execute this operation!'
        );
    }
}
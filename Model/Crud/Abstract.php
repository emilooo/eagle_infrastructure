<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for interface of crud
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Model_Crud_Abstract
extends Infrastructure_Model_Acl_Abstract
implements Infrastructure_Model_Crud_Interface
{
    /**
     * Stores the name of create form
     * 
     * @var string
     */
    public $createForm = 'Create';
    
    /**
     * Stores the name of update form
     * 
     * @var string
     */
    public $updateForm = 'Update';
    
    public function createItem($dataToSave)
    {
        if (!$this->checkAcl('createItem')) {
            $this->_throwAccessDeniedException();
        }
        
        $getCreateFormResult = $this->getForm($this->createForm);
        if ($getCreateFormResult->isValid($dataToSave)) {
            $transformColumnsResult
                = $this->_transformColumns($getCreateFormResult->getValues());
            return $this->getResource($this->getResourceId())
                ->createItem($transformColumnsResult);
        }
        
        return false;
    }
    
    public function readManyItems($pageNumber = null)
    {
        if (!$this->checkAcl('readManyItems')) {
            $this->_throwAccessDeniedException();
        }
        
        return $this->getResource($this->getResourceId())
            ->readManyItems($pageNumber);
    }
    
    /**
     * Throws the execute of not premissions to execute operation
     * 
     * @throws Infrastructure_Acl_AccessDenied
     */
    protected function _throwAccessDeniedException()
    {
        throw new Infrastructure_Acl_AccessDenied(
            'You haven\'t permissions to execute this operation!'
        );
    }
    
    /**
     * This method will be executed before action of create and update.
     * Through this method you can append other fields that you would like
     * to add or update in the database structure
     *  
     * @param array $dataToConvert
     * @return array
     */
    protected function _transformColumns($dataToConvert)
    {
        return $dataToConvert;
    }
    
    /**
     * Returns the data without column name for index of array. If the content
     * of the array exists two same indexes, then first index has a name from
     * name of a column but second index has a name from name of the table.
     * 
     * array (
     *      'column_first' => 'first', <- name from column
     *      'column_second' => 'second'
     *      'column_first' => 'column' <- name from table
     * )
     * 
     * @param array $dataToConvert
     * @return array
     */
    public function toArrayWithoutColumnName($dataToConvert)
    {
        $convertedData = array();
        foreach ($dataToConvert as $dataIndex => $dataValue) {
            $newIndex = preg_replace("/[a-z]+_/", "", $dataIndex);
            if (array_key_exists($newIndex, $convertedData)) {
                $tableNames = array();
                $getTableNameResult
                    = preg_match("/^[a-z]+/", $dataIndex, $tableNames);
                if ($getTableNameResult) {
                    $newIndex = $tableNames[0];
                    $convertedData[$newIndex] = $dataValue;
                } else {
                    $convertedData[$dataIndex] = $dataValue;
                }
            } else {
                $convertedData[$newIndex] = $dataValue;
            }
        }
        
        return $convertedData;
    }
}

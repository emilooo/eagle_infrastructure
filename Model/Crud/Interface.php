<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for crud model
 * 
 * @category Infrastructure
 * @package Infrastructure_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @deprecated 2.8
 */
interface Infrastructure_Model_Crud_Interface
extends Infrastructure_Model_Interface
{
    /**
     * Deletes the item from database structure
     * 
     * @param int $id
     * @return int Number of deletes rows
     */
    public function deleteItem($id);
    
    /**
     * Returns the instance of item from database structure if exist
     * 
     * @param int $id Number of item
     * @return Zend_Db_Table_Row|null
     */
    public function readOneItem($id);
    
    /**
     * Updates the item
     * 
     * @param int $id Number of id item
     * @param array
     * @return int Returns the number of updates items
     */
    public function updateItem($id, $dataToUpdate);
}
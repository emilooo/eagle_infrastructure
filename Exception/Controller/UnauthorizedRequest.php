<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Exception
 * @subpackage Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Exception for unauthorized request
 * 
 * @category Infrastructure
 * @package Infrastructure_Exception
 * @subpackage Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Exception_Controller_UnauthorizedRequest
extends Infrastructure_Exception_Base
{
}
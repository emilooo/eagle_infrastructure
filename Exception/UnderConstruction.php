<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Exception
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Exception for error 404
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Exception
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Exception_UnderConstruction
extends Infrastructure_Exception_Base
{
}

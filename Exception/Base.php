<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Exception
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Base system infrastructure exception
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Exception
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Exception_Base
extends \Exception
{
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Exception
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Exception for error in action of controller
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Exception
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Exception_ActionHasError
extends Infrastructure_Exception_Base
{
}
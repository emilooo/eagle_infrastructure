<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Test_Functional_Controller
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing if response code request
 * is equal with expected code response.
 * 
 * @category Infrastructure
 * @package Infrastructure_Test_Functional_Controller
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Rest_Error_ErrorAction_IsExpectedResponseCodeTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * Returns the expected response code number
     * 
     * @return int
     */
    abstract protected function _getExpectedCodeResponse();
    
    public function test_dispachRequestWhichHasError_responseCode()
    {
        $moduleName = $this->_getModuleName();
        $controllerName = $this->_getControllerName();
        $actionName = $this->_getActionName();
        $expectedResponseCode = $this->_getExpectedCodeResponse();
        $this->dispatch("/{$moduleName}/{$controllerName}/{$actionName}");
            $responseCode = $this->getResponse()->getHttpResponseCode();
        
        $this->assertTrue(($responseCode == $expectedResponseCode));
    }
}
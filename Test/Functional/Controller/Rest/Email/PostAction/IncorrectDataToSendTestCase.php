<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Test_Functional_Controller
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the action of post inside
 * the rest controller. Testing the behavior when process of sending email
 * message has ended fail.
 * 
 * @category Infrastructure
 * @package Infrastructure_Test_Functional_Controller
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Rest_Email_PostAction_IncorrectDataToSendTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    public function test_sendInCorrectDataToSend_true()
    {
        $moduleName = $this->_getModuleName();
        $controllerName = $this->_getControllerName();
        $actionName = $this->_getActionName();
        $dataToSend = json_encode($this->_getIncorrectDataToSend());
        $this->getRequest()->setMethod('POST')->setRawBody($dataToSend);
        
        $this->dispatch("/{$moduleName}/{$controllerName}/{$actionName}");
            $isException = $this->getResponse()->isException();
                if ($isException) {
                    $exceptionMessage
                        = $this->getResponse()->getException()[0]->getMessage();
                    return $this->fail($exceptionMessage);
                }
            $isCorrectResponseCode
                = ($this->getResponse()->getHttpResponseCode() == 406);
        
        $this->assertFalse($isException);
        $this->assertTrue($isCorrectResponseCode);
    }
    
    /**
     * Returns the incorrect data to send as array
     * 
     * @return array
     */
    abstract protected function _getIncorrectDataToSend();
}
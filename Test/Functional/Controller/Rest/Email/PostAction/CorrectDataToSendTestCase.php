<?php
/**
 * @category Infrastructure
 * @package Infrastructure_Test_Functional_Controller
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the interface to testing the bahavior when the email message has sended correctly with level of rest interface through  * post action.
 * @category Infrastructure
 * @package Infrastructure_Test_Functional_Controller
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Rest_Email_PostAction_CorrectDataToSendTestCase
extends Infrastructure_Test_Functional_Controller_Rest_Email_TestCase
{
    public function setUp()
    {
        parent::setUp();
        
        $this->_removeAllEmails();
    }
    
    public function tearDown()
    {
        parent::tearDown();
        
        $this->_removeAllEmails();
    }
    
    /**
     * Verifies the process does e-mail message has sended correctly
     * 
     * Steps:
     * - verifies does exception not exists,
     * - verifies does http response code is correctly,
     * - verifies does content of message is correctly
     * 
     * @return void
     */
    public function test_sendCorrectDataToSend_true()
    {
        $moduleName = $this->_getModuleName();
        $controllerName = $this->_getControllerName();
        $actionName = $this->_getActionName();
        $dataToSendAsArray = $this->_getCorrectDataToSend();
        $dataToSendAsJson = json_encode($dataToSendAsArray);
        $this->getRequest()->setMethod('POST')->setRawBody($dataToSendAsJson);
        
        $this->dispatch("/{$moduleName}/{$controllerName}/{$actionName}");
            $isException = $this->getResponse()->isException();
                if ($isException) {
                    $exceptionMessage
                        = $this->getResponse()->getException()[0]->getMessage();
                    return $this->fail($exceptionMessage);
                }
            $isCorrectResponseCode
                = ($this->getResponse()->getHttpResponseCode() == 201);
            $getEmailFiles = $this->_readAllEmails();
            $isCorrectCountOfEmail = (count($getEmailFiles) == 1);
        
        $this->assertFalse($isException);
        $this->assertTrue($isCorrectResponseCode);
        $this->assertTrue($isCorrectCountOfEmail);
        $this->assertContains(
            $dataToSendAsArray["sender"],
            $getEmailFiles[0]->getHeader('from')
        );
        $this->assertContains(
            $dataToSendAsArray["subject"],
            $getEmailFiles[0]->getHeader('subject')
        );
        $this->assertContains(
            $dataToSendAsArray["content"],
            $getEmailFiles[0]->getContent()
        );
    }
    
    /**
     * Returns the incorrect data to send as array
     * 
     * @return array
     */
    abstract protected function _getCorrectDataToSend();
}

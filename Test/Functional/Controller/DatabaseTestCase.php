<?php
/**
 * Copyright (C) 2015 emilooo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category Test
 * @package Test_Infrastructure_Fuctional
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic interface for testing the behavior of http controllers
 * that use database connections
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Fuctional
 * @subpackage Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_DatabaseTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * Stores the instance of zend database connection
     */
    protected $_connection;
    
    public function setUp()
    {
        parent::setUp();
        $this->_prepareDatabase();
    }
    
    /**
     * Returns the instance of database connection
     * 
     * @return PHPUnit_Extensions_Database_DB_DefaultDatabaseConnection
     */
    public function getConnection()
    {
        if ($this->_connection == null) {
            $getConfigurationResult = $this->_getConfiguration();
            $zendConnection = Zend_Db::factory(
                $getConfigurationResult->resources->db
            );
            $connection
            = new PHPUnit_Extensions_Database_DB_DefaultDatabaseConnection(
                $zendConnection->getConnection()
            );
            $this->_connection = $connection;
            Zend_Db_Table_Abstract::setDefaultAdapter($zendConnection);
        }
        
        return $this->_connection;
    }
    
    /**
     * Retruns the content of fixture file
     * 
     * @abstract
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    abstract public function getDataSet();
    
    /**
     * Prepares the database structure to testing
     * through initiates fixture file
     * 
     * @return void
     */
    private function _prepareDatabase()
    {
        $dataSet = $this->getDataSet();
        
        if ($dataSet instanceof PHPUnit_Extensions_Database_DataSet_IDataSet) {
          $setupOperation
                = PHPUnit_Extensions_Database_Operation_Factory::CLEAN_INSERT();
          $setupOperation->execute($this->getConnection(), $dataSet);
        }
    }
}
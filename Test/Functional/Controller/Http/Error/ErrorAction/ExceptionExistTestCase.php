<?php
/**
 * This source file is part of content management system
 *
 * @category Core
 * @package Core_Test_Functional_Controller_ErrorController
 * @subpackage ErrorAction
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for testing the behavior of method
 * of errorAction when exception occurs
 * 
 * @category Core
 * @package Core_Test_Functional_Controller_ErrorController
 * @subpackage ErrorAction
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Error_ErrorAction_ExceptionExistTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    public function test_errorAction_exceptionOccursInAction_true()
    {
        $moduleName = $this->_getModuleName();
        $controllerName = $this->_getControllerName();
        $actionName = $this->_getActionName();
        
        $this->dispatch(
            $moduleName . '/' . $controllerName . '/' . $actionName . '/'
        );
        $expectedControllerName = $this->_getExpectedControllerName();
        $actualControllerName = $this->getRequest()->getControllerName();
        $expectedActionName = $this->_getExpectedActionName();
        $actualActionName = $this->getRequest()->getActionName();
        
        
        $this->assertEquals($expectedControllerName, $actualControllerName);
        $this->assertEquals($expectedActionName, $actualActionName);
        $this->assertQuery('html head title');
        $this->assertQuery('html body header.row h1');
        $this->assertQuery('html body main.row p');
        $this->assertQuery('html body main.row section#errors');
    }
    
    /**
     * Returns the expected controller name
     * 
     * @abstract
     * @return string
     */
    abstract protected function _getExpectedControllerName();
    
    /**
     * Returns the expected action name
     * 
     * @abstract
     * @return string
     */
    abstract protected function _getExpectedActionName();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of register
 * when value of token is not authorized
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Auth_RegisterAction_UnauthorizedRequestTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * Executes the action of register with unauthorized token
     */
    public function test_registerAction_aunathorizedRequest_True()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getPostResult = $this->_getPost();
        $getTokenResult = $this->_getToken();
        $this->getRequest()->setMethod('POST')->setPost($getPostResult);
        
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult . '?token=' . $getTokenResult
        );
        $isException = $this->getResponse()->isException();
        
        $this->assertFalse($isException);
        $this->assertQueryContentContains(
            '#messages',
            'This request is unauthorized please try again!'
        );
    }
    
    /**
     * Returns the data of post
     * 
     * @return array
     */
    abstract protected function _getPost();
    
    /**
     * Returns the value of token
     * 
     * @return string
     */
    abstract protected function _getToken();
}
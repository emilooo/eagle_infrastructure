<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of login
 * in the http auth controller when user is correct login
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Auth_LoginAction_UserLoggedTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * Executes the action of login
     */
    public function test_loginAction_userLogged_True()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getPostResult = $this->_getPost();
        $this->getRequest()->setMethod('POST')->setPost($getPostResult);
        $this->_login();
        
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult
        );
        $isException = $this->getResponse()->isException();
        $this->_logout();
        
        $this->assertFalse($isException);
        $this->assertRedirect();
    }
    
    public function tearDown()
    {
        parent::tearDown();
        
        $this->getConnection()->getConnection()->exec("DELETE FROM users");
    }
    
    /**
     * Login the user
     */
    abstract protected function _login();
    
    /**
     * Logout the user
     */
    abstract protected function _logout();
}
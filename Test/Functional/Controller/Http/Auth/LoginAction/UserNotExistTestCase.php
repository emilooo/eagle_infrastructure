<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of login
 * in the http auth controller when data of post are correct but user not
 * exist in the system
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Auth_LoginAction_UserNotExistTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * Executes the action of login with correct data of post but
     * this credentials not exists in the system
     */
    public function test_loginAction_userNotExist_True()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getPostResult = $this->_getPost();
        $this->getRequest()->setMethod('POST')->setPost($getPostResult);
        
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult
        );
        $isException = $this->getResponse()->isException();
        
        $this->assertFalse($isException);
        $this->assertQuery('body #messages');
    }
    
    /**
     * Returns the correct data of post for not exist user
     * 
     * @return array
     */
    abstract protected function _getPost();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of logout
 * in the http auth controller when user not logged
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Auth_LogoutAction_UserUnauthorizedTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * Executes the action of logout
     */
    public function test_loginAction_userUnauthorized_True()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getLoginUrl = $this->_getLoginUrl();
        
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult
        );
        $isException = $this->getResponse()->isException();
        
        $this->assertFalse($isException);
        $this->assertRedirectTo($getLoginUrl);
    }
    
    /**
     * Returns the login url path
     * 
     * @return string
     */
    abstract protected function _getLoginUrl();
}
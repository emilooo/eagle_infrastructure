<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of logout
 * in the http auth controller when user is logged
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Auth_LogoutAction_UserAuthorizedTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * Executes the action of logout
     */
    public function test_logoutAction_userAuthorized_true()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getLoginUrl = $this->_getLoginUrl();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        
        $this->assertFalse($isException);
        $this->assertRedirectTo($getLoginUrl);
    }
    
    public function tearDown()
    {
        parent::tearDown();
        
        $this->getConnection()->getConnection()->exec('DELETE FROM users');
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
    
    /**
     * Returns the login url path
     * 
     * @return string
     */
    abstract protected function _getLoginUrl();
}
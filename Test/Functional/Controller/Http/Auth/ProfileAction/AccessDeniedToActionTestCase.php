<?php
/**
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of index
 * when try reads the resource but access to the method is denied
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Auth_ProfileAction_AccessDeniedToActionTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * This test verifies the reaction of profileAction in the crud controller
     * when request hasn't access to the action
     */
    public function test_profileAction_AccessDeniedToActon_Redirect()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getLogin = $this->_getLogin();
        
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult . '/login/' . $getLogin
        );
        
        $this->assertRedirect();
    }
    
    /**
     * Returns the information about user profile
     * 
     * @return string
     */
    abstract protected function _getLogin();
}
<?php
/**
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the basic interface for testing the behavior for action of profile
 * when try gets the user of profile through exist login
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Auth_ProfileAction_ExistLoginTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of profileAction in the auth controller
     * when login of user not exist.
     */
    public function test_profileAction_InsertExistLogin_Body()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getLogin = $this->_getLogin();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult . '/login/' . $getLogin
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        if ($isException) {
            return $this->fail(
                $this->getResponse()->getException()[0]->getMessage()
            );
        }
        
        $this->assertFalse($isException);
        $this->assertQuery('body');
    }
    
    /**
     * Executes the process of login user
     */
    abstract protected function _login();
    
    /**
     * Executes the process of logout user
     */
    abstract protected function _logout();
    
    /**
     * Returns the information about user profile
     * 
     * @return string
     */
    abstract protected function _getLogin();
}
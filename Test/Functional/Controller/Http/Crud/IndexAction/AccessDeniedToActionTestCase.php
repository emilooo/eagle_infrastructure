<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of index
 * when try reads the resource but access to the method is denied
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_IndexAction_AccessDeniedToActionTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * This test verifies the reaction of readAction in the crud controller
     * when request hasn't access to the action
     */
    public function test_readAction_AccessDeniedToActon_Redirect()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getPageResult = $this->_getPage();
        
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult . '/page/' . $getPageResult . '/'
        );
        
        $this->assertRedirect();
    }
    
    /**
     * Returns the number of page
     * 
     * @return int
     */
    abstract protected function _getPage();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of index
 * when try reads the part of exist resources with number of page
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_IndexAction_PageNumberExistTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of indexAction in the crud controller
     * when page number from request is send and request has access
     * to the action
     */
    public function test_indexAction_SendExistPageNumber_True()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getPageNumberResult = $this->_getPage();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult .'/page/' . $getPageNumberResult . '/'
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        if ($isException) {
            $this->fail($this->getResponse()->getException()[0]->getMessage());
        }
        
        $this->assertFalse($isException);
        $this->assertQuery('html body');
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
    
    /**
     * Returns the number of page
     * 
     * @return int
     */
    abstract protected function _getPage();
}
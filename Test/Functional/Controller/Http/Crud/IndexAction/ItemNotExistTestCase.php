<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of index
 * when I try read all resources when they not exist
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_IndexAction_ItemNotExistTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of indexAction in the crud controller
     * when try read the not exist resources and request has access
     * to the action
     */
    public function test_indexAction_notExistItem_true()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult
            . '/'. $getActionNameResult .'/'
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        
        $this->assertFalse($isException);
        $this->assertQuery('html body');
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
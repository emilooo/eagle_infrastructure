<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of index
 * when try reads the all resources without number of page
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_IndexAction_EmptyPageNumberTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of indexAction in the crud controller
     * when page number from request is not send and request
     * has access to the action
     */
    public function test_indexAction_pageNumberNotSend_true()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
            if ($isException) {
                return $this->assertTrue($this->_isExpectedException());
            }
        $this->assertFalse($isException);
        $this->assertQuery('html body');
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
    
    /**
     * Verifies if exception is expected
     * 
     * @return boolean
     */
    private function _isExpectedException()
    {
        $getExceptionResult
            = $this->getResponse()->getException()[0];
        
        return (
            $this->_getExceptedExceptionMessage()
                == $getExceptionResult->getMessage()
        );
    }
    
    /**
     * Returns the content of expected exception message if exists
     * 
     * @return string
     */
    private function _getExceptedExceptionMessage()
    {
        return 'Argument 1 passed to Zend_View_Helper_PaginationControl::paginationControl() must be an instance of Zend_Paginator, instance of Zend_Db_Table_Rowset given';
    }
}
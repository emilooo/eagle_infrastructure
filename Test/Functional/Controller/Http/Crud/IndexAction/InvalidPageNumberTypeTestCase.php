<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of index
 * when try reads the part of resources when number of page is invalid
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_IndexAction_InvalidPageNumberTypeTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    public function test_indexAction_sendInvalidTypeOfPageNumber_true()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getPageResult = $this->_getPage();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult . '/page/' . $getPageResult
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        
        $this->assertTrue($isException);
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
    
    /**
     * Returns the number of page
     * 
     * @return string
     */
    abstract protected function _getPage();
}
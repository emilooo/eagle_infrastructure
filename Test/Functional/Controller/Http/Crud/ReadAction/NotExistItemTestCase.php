<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of read
 * in the http crud controller when access to action denied
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_ReadAction_NotExistItemTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of readAction in the crud controller
     * when id of record not send and request has access to the action
     */
    public function test_readAction_readNotExistItem_True()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        $getIdParam = $this->_getId();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult . '/id/' . $getIdParam . '/'
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        
        $this->assertTrue($isException);
    }
    
    /**
     * Returns the number of id
     * 
     * @return int
     */
    abstract protected function _getId();
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
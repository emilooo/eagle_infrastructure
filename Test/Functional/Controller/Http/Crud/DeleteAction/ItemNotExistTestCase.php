<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of delete
 * when try deletes the not exist resource
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_DeleteAction_ItemNotExistTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of deleteAction in the crud controller
     * when try deleting not exist resource
     */
    public function test_deleteAction_notExistItem_True()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionResult = $this->_getActionName();
        $getIdResult = $this->_getId();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionResult . '/id/' . $getIdResult
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        $isExceptionMessage = $this->getResponse()
            ->getExceptionByMessage('Deleting of resource has failed!');
        
        $this->assertTrue($isException);
        $this->assertNotEmpty($isExceptionMessage);
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
    
    /**
     * Returns the data of post
     * 
     * @return int
     */
    abstract protected function _getId();
}
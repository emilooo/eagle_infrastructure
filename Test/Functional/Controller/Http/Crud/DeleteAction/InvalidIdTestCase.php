<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of delete
 * in the http crud controller when id of item is invalid
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_DeleteAction_InvalidIdTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of deleteAction in the crud controller
     * when id of item is invalid and request has access to the action
     */
    public function test_deleteAction_InvalidId_True()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getIdResult = $this->_getId();
        $getActionNameResult = $this->_getActionName();
        
        $this->_login();
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult . '/id/' . $getIdResult
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        $isExceptionMessage = $this->getResponse()
            ->getExceptionByMessage('assert(): Assertion failed');
        
        $this->assertTrue($isException);
        $this->assertNotEmpty($isExceptionMessage);
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
    
    /**
     * Returns the number of id
     * 
     * @return int
     */
    abstract protected function _getId();
}
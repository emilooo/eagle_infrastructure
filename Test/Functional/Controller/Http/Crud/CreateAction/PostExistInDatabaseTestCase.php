<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of create
 * in the http crud controller when data of post exist
 * in the database structure
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_CreateAction_PostExistInDatabaseTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of createAction in the crud controller
     * when data of post is exist in the database and request
     * has access to the action
     */
    public function test_createAction_PostExistInDatabase_True()
    {
        $getPostResult = $this->_getPost();
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        
        $this->_login();
        $this->getRequest()->setMethod('POST')->setPost($getPostResult);
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        
        $this->assertFalse($isException);
        $this->assertQuery('body form .errors');
    }
    
    /**
     * Retruns the data of post
     * 
     * @return array
     */
    abstract protected function _getPost();
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
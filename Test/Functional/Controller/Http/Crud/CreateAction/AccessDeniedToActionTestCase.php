<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of create
 * in the http crud controller when access to action denied
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_CreateAction_AccessDeniedToActionTestCase
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * This test verifies the reaction of createAction in the crud controller
     * when request hasn't access to the action
     */
    public function test_createAction_AccessDeniedToActon_Redirect()
    {
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult
        );
        
        $this->assertRedirect();
    }
}
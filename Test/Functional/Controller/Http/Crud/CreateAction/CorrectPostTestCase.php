<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the behavior of action of create
 * in the http crud controller when data of post are correct
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_Http_Crud_CreateAction_CorrectPostTestCase
extends Infrastructure_Test_Functional_Controller_DatabaseTestCase
{
    /**
     * This test verifies the reaction of createAction in the crud controller
     * when data of post is correct and request has access to the action
     */
    public function test_createAction_CorrectPost_True()
    {
        $getPostResult = $this->_getPost();
        $getModuleNameResult = $this->_getModuleName();
        $getControllerNameResult = $this->_getControllerName();
        $getActionNameResult = $this->_getActionName();
        
        $this->_login();
        $this->getRequest()->setMethod('POST')->setPost($getPostResult);
        $this->dispatch(
            $getModuleNameResult . '/' . $getControllerNameResult . '/'
            . $getActionNameResult
        );
        $this->_logout();
        $isException = $this->getResponse()->isException();
        if ($isException) {
            $this->fail($this->getResponse()->getException()[0]->getMessage());
        }
        
        $this->assertFalse($isException);
        $this->assertRedirect();
    }
    
    /**
     * Login user to system
     * 
     * @return boolen
     */
    abstract protected function _login();
    
    /**
     * Logout user with system
     * 
     * @return boolen
     */
    abstract protected function _logout();
}
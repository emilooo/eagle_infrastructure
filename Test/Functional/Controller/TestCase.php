<?php
/**
 * Copyright (C) 2015 emilooo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category Test
 * @package Test_Infrastructure_Fuctional
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic interface for testing the behavior of http controllers
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Fuctional
 * @subpackage Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Functional_Controller_TestCase
extends Zend_Test_PHPUnit_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
        
        $application = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_CONFIGURATION_FILE
        );
        
        $application->bootstrap();
    }
    
    /**
     * Returns the configuration of application
     * 
     * @return Zend_Config_Ini
     */
    protected function _getConfiguration()
    {
        $currentApplicationConfiguration = new Zend_Config_Ini(
            APPLICATION_CONFIGURATION_FILE,
            APPLICATION_ENV
        );
        
        return $currentApplicationConfiguration;
    }
    
    /**
     * Returns the name of module
     * 
     * @return string
     */
    abstract protected function _getModuleName();
    
    /**
     * Returns the name of controller
     * 
     * @return string
     */
    abstract protected function _getControllerName();
    
    /**
     * Returns the name of action
     * 
     * @return string
     */
    abstract protected function _getActionName();
}
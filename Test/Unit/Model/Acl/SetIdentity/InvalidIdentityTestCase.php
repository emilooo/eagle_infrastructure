<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try sets the invalid identity to method of setIdentity
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Model_Acl_SetIdentity_InvalidIdentityTestCase
extends Infrastructure_Test_Unit_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $model = new $getModelFullClassNameResult();
        
        return $model;
    }
    
    public function test_setIdentity_InsertInvalidIdentity_Exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $makeModelResult->setIdentity(
                $this->_getIdentity()
            );
        } catch (Exception $exception) {
            $getExceptionMessageResult = $exception->getMessage();
            $isCorrectMessage = ($getExceptionMessageResult
                === 'assert(): Assertion failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns then invalid identity
     * 
     * @return int
     */
    abstract protected function _getIdentity();
}


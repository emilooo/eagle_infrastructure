<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try checks the privilage to action through invalid name of action
 * in the method of checkAction
 * 
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Model_Acl_CheckAcl_InvalidActionNameTestCase
extends Infrastructure_Test_Unit_Model_TestCase
{
    public function makeModel()
    {
        $getModdelFullClassNameResult = $this->_getModelFullClassName();
        $model = new $getModdelFullClassNameResult(
            array(
                'acl' => $this->_makeAclMock()
            )
        );
        
        return $model;
    }
    
    public function test_checkAcl_InsertInvalidActionName()
    {
        try {
            $makeModelResult = $this->makeModel();
            $makeModelResult->checkAcl(
                $this->_getActionName()
            );
        } catch (Exception $exception) {
            $getExceptionMessageResult = $exception->getMessage();
            $isCorrectMessage = ($getExceptionMessageResult
                === 'assert(): Assertion failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the invalid name of action that isn't string
     */
    abstract protected function _getActionName();
    
    /**
     * Returns the instance of acl mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        
        return $aclMock;
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try gets the exist identity.
 * 
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Model_Acl_GetIdentity_ExistIdentityTestCase
extends Infrastructure_Test_Unit_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $model = new $getModelFullClassNameResult(
            array(
                'identityRole'=>$this->_makeIdentityMock()
            )
        );
        
        return $model;
    }
    
    public function test_getIdentity_IdentityHasDefined_True()
    {
        $model = $this->makeModel();
        $getIdentityResult = $model->getIdentity();
        $isCorrectIdentityResult = ($getIdentityResult
            instanceof Zend_Acl_Role_Interface);
        
        $this->assertTrue($isCorrectIdentityResult);
    }
    
    /**
     * Returns the instance of role
     * 
     * @return Zend_Acl_Role_Interface
     */
    private function _makeIdentityMock()
    {
        $identityMock = Mockery::mock('Zend_Acl_Role_Interface');
        
        return $identityMock;
    }
}


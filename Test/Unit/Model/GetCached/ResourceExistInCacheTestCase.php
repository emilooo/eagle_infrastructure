<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try gets the exist resource from cache.
 * 
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Model_GetCached_ResourceExistInCacheTestCase
extends Infrastructure_Test_Unit_Model_TestCase
{
    public function makeModel()
    {
        $getModelClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelClassNameResult(
            array(
                'cache'=>$this->_makeModelCache()
            )
        );
        
        return $modelInstance;
    }
    
    /**
     * Verify output data when try get cached resource
     */
    public function test_getCached_ResourceExistInCache_True()
    {
        $makeModelResoult = $this->makeModel();
        $getCachedResult = $makeModelResoult->getCached(
            $makeModelResoult->getResourceId()
        );
        $getSecondCachedResult = $makeModelResoult->getCached(
            $makeModelResoult->getResourceId()
        );
        $isCorrectCacheResult = ($getSecondCachedResult
            instanceof Infrastructure_Model_Cache_Abstract);
        
        $this->assertTrue($isCorrectCacheResult);
    }
    
    private function _makeModelCache()
    {
        $modelCacheMock = Mockery::mock('Infrastructure_Model_Cache_Abstract');
        $modelCacheMock
            ->shouldReceive('setTagged')
            ->with($this->_getModelShortClassName());
        
        return $modelCacheMock;
    }
}
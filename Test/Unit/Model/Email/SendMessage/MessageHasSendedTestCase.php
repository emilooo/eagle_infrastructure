<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of unit test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/'
    . 'TestCase.php';

/**
 * Implements the basic implementation for testing the method
 * of sendMessage in the email model. This test verifies the behavior
 * when input parameter of dataToSave is correct and email message
 * has correctly sended.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Model_Email_SendMessage_MessageHasSendedTestCase
extends Infrastructure_Test_Unit_Model_TestCase
{
    /**
     * Returns the name of form
     * 
     * @return Infrastructure_Form_Abstract
     */
    abstract protected function _getFormName();
    
    /**
     * Returns the invalid data to send. This return type can't be array
     * 
     * @return string|int
     */
    abstract protected function _getCorrectDataToSend();
    
    /**
     * Vierifies the behavior of method of sendMessage is data to send
     * are invalid
     */
    public function test_insertInvalidDataToSend_exception()
    {
        $dataToSend = $this->_getCorrectDataToSend();
        $emailModel = $this->makeModel();
        $sendMessageResult = $emailModel->sendMessage($dataToSend);
        
        $this->assertTrue(true);
    }
    
    /**
     * Returns the prepared email model
     * 
     * @return Infrastructure_Model_Email
     */
    public function makeModel()
    {
        $fullModelClassName = $this->_getModelFullClassName();
        $modelInstance = new $fullModelClassName(
            array(
                'acl' => $this->_makeAclMock(),
                'identity' => $this->_makeIdentityMock(),
                'forms'
                    => array($this->_getFormName() => $this->_makeFormMock()),
                "resources"
                    => array("Email" => $this->_makeEmailResourceMock())
            )
        );
        
        return $modelInstance;
    }
    
    /**
     * Returns instance of alc mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        $aclMock
            ->shouldReceive('isAllowed')
            ->andReturn(true);
        
        return $aclMock;
    }
    
    /**
     * Returns the instance of acl role mock
     * 
     * @return Zend_Acl_Role_Interface
     */
    private function _makeIdentityMock()
    {
        $identityMock = Mockery::mock('Zend_Acl_Role_Interface');
        
        return $identityMock;
    }
    
    /**
     * Returns the instance of form mock
     * 
     * @return Zend_Form
     */
    private function _makeFormMock()
    {
        $formMock = Mockery::mock('Infrastructure_Form_Abstract');
        $formMock
            ->shouldReceive('isValid')
            ->with($this->_getCorrectDataToSend())
            ->andReturn(true);
        $formMock
            ->shouldReceive('populate')
            ->with($this->_getCorrectDataToSend())
            ->andReturnNull();
;        
        return $formMock;
    }
    
    /**
     * Return instance of email resource mock
     * 
     * @return Infrastructure_Model_Resource_Email_Interface
     */
    private function _makeEmailResourceMock()
    {
        $emailMock
            = Mockery::mock("Infrastructure_Model_Resource_Email_Interface");
        $emailMock
            ->shouldReceive("sendMessage")
            ->with($this->_getCorrectDataToSend())
            ->andReturn(true);
        
        return $emailMock;
    }
}
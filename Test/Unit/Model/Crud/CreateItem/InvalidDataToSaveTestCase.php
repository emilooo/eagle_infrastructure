<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/'
    . 'TestCase.php';

/**
 * Implements the basic implementation for testing the method
 * of createItem in the crud model. When access to the method
 * is enabled and parameter of dataToSave is invalid.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Unit_Model_Crud_CreateItem_InvalidDataToSaveTestCase
extends Infrastructure_Test_Unit_Model_Crud_TestCase
{
    public function test_createItem_InvalidDataToSave_Exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $createItemResult = $makeModelResult->createItem(
                $this->_getInvalidDataToSave()
            );            
        } catch (Exception $ex) {
            $getMessageResult = $ex->getMessage();
            
            $isCorrectMessage = ($getMessageResult
                === 'Zend_Form::isValid expects an array');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the instance of crud model
     * 
     * @return Infrastructure_Model_Resource_Db_Table_Crud_Abstract
     */
    public function makeModel()
    {
        $getModelClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelClassNameResult(
            array(
                'acl' => $this->_makeAclMock(),
                'identity' => $this->_makeIdentityMock(),
                'forms' => array(
                    'Create' => $this->_makeCreateFormMock()
                )
            )
        );
        
        return $modelInstance;
    }
    
    /**
     * Returns incorrect data to save
     * 
     * @return array
     */
    abstract protected function _getInvalidDataToSave();
    
    /**
     * Returns instance of alc mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        $aclMock
            ->shouldReceive('isAllowed')
            ->andReturn(true);
        
        return $aclMock;
    }
    
    /**
     * Returns the instance of acl role mock
     * 
     * @return Zend_Acl_Role_Interface
     */
    private function _makeIdentityMock()
    {
        $identityMock = Mockery::mock('Zend_Acl_Role_Interface');
        
        return $identityMock;
    }
    
    /**
     * Returns the instance of form mock
     * 
     * @return Zend_Form
     */
    private function _makeCreateFormMock()
    {
        $createForm = Mockery::mock('Zend_Form');
        $createForm
            ->shouldReceive('isValid')
            ->with($this->_getInvalidDataToSave())
            ->andThrow(
                $this->_makeExceptionForInvalidDataToSave()
            );
        
        return $createForm;
    }
    
    private function _makeExceptionForInvalidDataToSave()
    {
        return new Exception('Zend_Form::isValid expects an array');
    }
}
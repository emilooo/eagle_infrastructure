<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/'
    . 'TestCase.php';

/**
 * Implements the basic implementation for testing the method
 * of deleteItems in the crud model. When access to the method is enabled
 * and item not exist.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Unit_Model_Crud_DeleteItem_ItemNotExistTestCase
extends Infrastructure_Test_Unit_Model_Crud_TestCase
{
    public function test_deleteItems_ItemNotExist_Exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $deleteItemsResult = $makeModelResult->deleteItem(
                $this->_getId()
            );
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            $getExceptionMessage = $ex->getMessage();
            $isCorrectMessage
                = ($getExceptionMessage === 'Item not exist');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the instance of crud model
     * 
     * @return Infrastructure_Model_Resource_Db_Table_Crud_Abstract
     */
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelFullClassNameResult(
            array(
                'acl' => $this->_makeAclMock(),
                'identity' => $this->_makeIdentityMock(),
                'resources' => array(
                    $this->_getModelShortClassName()
                        => $this->_makeResourceMock()
                )
            )
        );
        
        return $modelInstance;
    }
    
    /**
     * Returns the value of column
     * 
     * @return mixed
     */
    abstract protected function _getId();
    
    /**
     * Returns instance of alc mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        $aclMock
            ->shouldReceive('isAllowed')
            ->andReturn(true);
        
        return $aclMock;
    }
    
    /**
     * Returns the instance of acl role mock
     * 
     * @return Zend_Acl_Role_Interface
     */
    private function _makeIdentityMock()
    {
        $identityMock = Mockery::mock('Zend_Acl_Role_Interface');
        
        return $identityMock;
    }
    
    /**
     * Returns the instance of resource mock
     * 
     * @return Infrastructure_Model_Resource_Db_Table_Crud_Interface
     */
    private function _makeResourceMock()
    {
        $resourceMock = Mockery::mock(
            'Infrastructure_Model_Resource_Db_Table_Crud_Interface'
        );
        $resourceMock
            ->shouldReceive('deleteItems')
            ->andThrow(
                $this->_makeExceptionForNotExistItem()
            );
        
        return $resourceMock;
    }
    
    /**
     * Throrws the instance of exception for not exist item
     * 
     * @return \Infrastructure_Model_Resource_Db_Table_Row_NotExist
     */
    private function _makeExceptionForNotExistItem()
    {
        return new Infrastructure_Model_Resource_Db_Table_Row_NotExist(
            'Item not exist'
        );
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for testing the method of createItem in the crud model
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Unit_Model_Crud_TestCase
extends PHPUnit_Framework_TestCase
{
    /**
     * Retruns the instance of crud model
     * 
     * @return Infrastructure_Model_Resource_Db_Table_Crud_Interface
     */
    abstract public function makeModel();
    
    /**
     * Returns the full name of model class
     * 
     * @return string
     */
    protected abstract function _getModelFullClassName();
    
    /**
     * Returns the short name of model class
     * 
     * @return string
     */
    protected abstract function _getModelShortClassName();
}
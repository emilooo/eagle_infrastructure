<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/'
    . 'TestCase.php';

/**
 * ITesting the behavior of method of updateItem when input parameter
 * of id is invalid
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Unit_Model_Crud_UpdateItem_InvalidIdTestCase
extends Infrastructure_Test_Unit_Model_Crud_TestCase
{
    public function test_updateItem_invalidId_exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $updateItemsResult = $makeModelResult->updateItem(
                $this->_getId(),
                $this->_getDataToUpdate()
            );
        } catch (Exception $ex) {
            $getMessage = $ex->getMessage();
            $isCorrectMessage = ($getMessage
                === 'assert(): Assertion failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        return $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the instance of crud model
     * 
     * @return Infrastructure_Model_Resource_Db_Table_Crud_Abstract
     */
    public function makeModel()
    {
        $getModelClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelClassNameResult(
            array(
                'acl' => $this->_makeAclMock(),
                'identity' => $this->_makeIdentityMock()
            )
        );
        
        return $modelInstance;
    }
    
    /**
     * Returns the number of id
     * 
     * @return int
     */
    abstract protected function _getId();
    
    /**
     * Returns incorrect data to save
     * 
     * @return array
     */
    abstract protected function _getDataToUpdate();
    
    /**
     * Returns instance of alc mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        $aclMock
            ->shouldReceive('isAllowed')
            ->andReturn(true);
        
        return $aclMock;
    }
    
    /**
     * Returns the instance of acl role mock
     * 
     * @return Zend_Acl_Role_Interface
     */
    private function _makeIdentityMock()
    {
        $identityMock = Mockery::mock('Zend_Acl_Role_Interface');
        
        return $identityMock;
    }
}
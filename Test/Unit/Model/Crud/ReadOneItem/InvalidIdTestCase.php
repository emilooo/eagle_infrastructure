<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/'
    . 'TestCase.php';

/**
 * Implements the basic implementation for testing the method
 * of readOneItem in the crud model. When access to the method is enabled
 * and parameter of id is invalid.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Unit_Model_Crud_ReadOneItem_InvalidIdTestCase
extends Infrastructure_Test_Unit_Model_Crud_TestCase
{
    public function test_readOneItem_InsertInvalidId_Exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $readOneItemResult = $makeModelResult->readOneItem(
                $this->_getId()
            );
        } catch (Exception $ex) {
            $getExceptionMessage = $ex->getMessage();
            $isCorrectMessage
                = ($getExceptionMessage === 'assert(): Assertion failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the instance of crud model
     * 
     * @return Infrastructure_Model_Resource_Db_Table_Crud_Abstract
     */
    public function makeModel()
    {
        $getModelClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelClassNameResult(
            array(
                'acl' => $this->_makeAclMock(),
                'identity' => $this->_makeIdentityMock(),
                'resources' => array(
                    $this->_getModelShortClassName()
                        => $this->_makeResourceMock()
                )
            )
        );
        
        return $modelInstance;
    }
    
    /**
     * Returns the number of id
     * 
     * @return int
     */
    abstract protected function _getId();
    
    /**
     * Returns instance of alc mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        $aclMock
            ->shouldReceive('isAllowed')
            ->andReturn(true);
        
        return $aclMock;
    }
    
    /**
     * Returns the instance of acl role mock
     * 
     * @return Zend_Acl_Role_Interface
     */
    private function _makeIdentityMock()
    {
        $identityMock = Mockery::mock('Zend_Acl_Role_Interface');
        
        return $identityMock;
    }
    
    /**
     * Returns the instance of resource mock
     * 
     * @return Infrastructure_Model_Resource_Db_Table_Crud_Interface
     */
    private function _makeResourceMock()
    {
        $resourceMock = Mockery::mock(
            'Infrastructure_Model_Resource_Db_Table_Crud_Interface'
        );
        $resourceMock
            ->shouldReceive('readOneItem')
            ->andThrow($this->_makeExceptionForInvalidParam());
        
        return $resourceMock;
    }
    
    /**
     * Returns the instance of exception
     * 
     * @return \Exception
     */
    private function _makeExceptionForInvalidParam()
    {
        return new Exception('assert(): Assertion failed');
    }
}
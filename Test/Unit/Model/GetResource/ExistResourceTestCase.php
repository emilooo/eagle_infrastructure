<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try gets the exist resource.
 * 
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Model_GetResource_ExistResourceTestCase
extends Infrastructure_Test_Unit_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelFullClassNameResult(
            array(
                'resources' => array(
                    $this->_getResourceName() => $this->_makeResourceMock()
                )
            )
        );
        
        return $modelInstance;
    }
    
    public function test_getCached_InsertExistResourceName_True()
    {
        $makeModelResult = $this->makeModel();
        $getResourceResult = $makeModelResult->getResource(
            $this->_getResourceName()
        );
        $isCorrectResource = ($getResourceResult
            instanceof Infrastructure_Model_Resource_Interface);
        
        $this->assertTrue($isCorrectResource);
    }
    
    /**
     * Returns the name of resource
     * 
     * @return string
     */
    abstract protected function _getResourceName();
    
    /**
     * Returns the instance of resource mock
     * 
     * @return Infrastructure_Model_Resource_Db_Table_Crud_Interface
     */
    private function _makeResourceMock()
    {
        $resourceMock = Mockery::mock(
            'Infrastructure_Model_Resource_Db_Table_Crud_Interface'
        );
        
        return $resourceMock;
    }
}

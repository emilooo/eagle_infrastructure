<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try sets the correct properties through method of setProperties.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Model_SetProperties_CorrectPropertiesTestCase
extends Infrastructure_Test_Unit_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelFullClassNameResult();
        
        return $modelInstance;
    }
    
    public function test_getCached_InsertInvalidOptions_True()
    {
        $makeModelResult = $this->makeModel();
        $setPropertiesResult = $makeModelResult->setProperties(
            $this->_getOptions()
        );
        $isCorrectCache = (
            $makeModelResult->getCached($this->_getModelShortClassName())
            instanceof Infrastructure_Model_Cache
        );
        
        $this->assertTrue($isCorrectCache);
    }
    
    /**
     * Returns the options for cache property
     * 
     * @return string
     */
    abstract protected function _getOptions();
}

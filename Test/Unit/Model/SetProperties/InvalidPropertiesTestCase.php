<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try sets the invalid properties through method of setProperties.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Model_SetProperties_InvalidPropertiesTestCase
extends Infrastructure_Test_Unit_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelFullClassNameResult();
        
        return $modelInstance;
    }
    
    public function test_getCached_InsertInvalidOptions_Exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $getResourceResult = $makeModelResult->setProperties(
                $this->_getOptions()
            );
        } catch (Exception $exception) {
            $getExceptionMessage = $exception->getMessage();
            $isCorrectMessage = ($getExceptionMessage
                === 'assert(): Assertion failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the options
     * 
     * @return string
     */
    abstract protected function _getOptions();
}

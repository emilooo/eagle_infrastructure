<?php
/**
 * @category Infrastructure
 * @package Infrastructure_Test_Unit_Service_Email
 * @subpackage SendMessage
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Service/Email/'
    . 'TestCase.php';

/**
 * Provides the interface for testing the behavior of sendMessage method
 * when email message has correct sended
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Test_Unit_Service_Email
 * @subpackage SendMessage
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Service_Email_SendMessage_EmailHasSendedTestCase
extends Infrastructure_Test_Unit_Service_Email_TestCase
{
    /**
     * Returns the e-mail sender
     * 
     * @return string
     */
    abstract protected function _getSender();
    
    /**
     * Returns the e-mail subject
     * 
     * @return string
     */
    abstract protected function _getSubject();
    
    /**
     * Returns the e-mail content
     * 
     * @return string
     */
    abstract protected function _getContent();
    
    /**
     * Puts the correct data to send and verifies does e-mail message has
     * correctly sended
     * 
     * @return void
     */
    public function test_putCorrectDataToSend_true()
    {
        $emailSender = $this->_getSender();
        $emailSubject = $this->_getSubject();
        $emailContent = $this->_getContent();
        
        $serviceInstance = $this->_makeServiceInstance();
        $sendEmailResult = $serviceInstance
            ->sendMessage($emailSender, $emailSubject, $emailContent);
        
        $this->assertTrue($sendEmailResult);
    }
    
    /**
     * Returns the instance of e-mail service
     * 
     * @return Infrastructure_Service_Email_Interface
     */
    private function _makeServiceInstance()
    {
        $serviceClassName = $this->_getFullServiceClassName();
        $serviceInstance = new $serviceClassName(
            array(
                'model' => $this->_makeModelMock()
            )
        );
            assert(
                $serviceInstance
                instanceof Infrastructure_Service_Email_Interface
            );
        
        return $serviceInstance;
    }
    
    /**
     * Returns the fake instance of model
     * 
     * @return Infrastructure_Model_Email
     */
    private function _makeModelMock()
    {
        $formMock = $this->_makeFormMock();
        $modelMock = Mockery::mock("Infrastructure_Model_Email");
        $modelMock
            ->shouldReceive('sendMessage')
            ->with(array(
                'sender' => $this->_getSender(),
                'subject' => $this->_getSubject(),
                'content' => $this->_getContent()
            ))
            ->andReturn(true);
        $modelMock
            ->shouldReceive('getForm')
            ->with('Message')
            ->andReturn($formMock);
        
        return $modelMock;
    }
    
    /**
     * Returns the fake instance of form
     * 
     * @return Infrastructure_Form_Abstract
     */
    private function _makeFormMock()
    {
        $formMock = Mockery::mock("Infrastructure_Form_Abstract");
        
        return $formMock;
    }
}
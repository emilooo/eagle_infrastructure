<?php
/**
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for testing the email services
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Unit
 * @subpackage Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Unit_Service_Email_TestCase
extends PHPUnit_Framework_TestCase
{
    /**
     * Returns the name of full service class name
     * 
     * @return string
     */
    abstract protected function _getFullServiceClassName();
}
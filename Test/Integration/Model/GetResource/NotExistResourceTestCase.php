<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the interface for testing the behavior when try gets
 * the not exist resource through correct name of resource
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_GetResource_NotExistResourceTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    /**
     * @expectedException Infrastructure_Model_InvalidOperation
     */
    public function test_getResource_InsertNotExistResourceName_Exception()
    {
        $makeModelResult = $this->makeModel();
        $makeModelResult->getResource(
            $this->_getResourceName()
        );
    }
    
    /**
     * Returns the name of resource
     * 
     * @return string
     */
    abstract protected function _getResourceName();
}
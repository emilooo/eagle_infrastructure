<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the interface for testing the behavior when try gets
 * the exist resource through correct name of resource
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_GetResource_ExistResourceTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_getResource_InsertExistResourceName_True()
    {
        $makeModelResult = $this->makeModel();
        $getResourceResult = $makeModelResult->getResource(
            $this->_getResourceName()
        );
        $isCorrectResource = ($getResourceResult
            instanceof Infrastructure_Model_Resource_Interface);
        
        $this->assertTrue($isCorrectResource);
    }
    
    /**
     * Returns the name of resource
     * 
     * @return string
     */
    abstract protected function _getResourceName();
}
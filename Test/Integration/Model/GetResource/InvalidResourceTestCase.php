<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the interface for testing the behavior when try gets
 * the resource through invalid name of resource
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_GetResource_InvalidResourceTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_getResource_InsertInvalidResourceName_Exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $makeModelResult->getResource(
                $this->_getResourceName()
            );            
        } catch (Exception $exception) {
            $getExceptionMessage = $exception->getMessage();
            $isCorrectMessage = ($getExceptionMessage
                == 'assert(): Parameter: resourceName is bad! failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the name of resource
     * 
     * @return string
     */
    abstract protected function _getResourceName();
}

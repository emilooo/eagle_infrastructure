<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the method of readManyOtems
 * when try get the resources from database structure through correct
 * input parameter of pageNumber.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Crud_ReadManyItems_ItemsWithoutPageParameterExistsTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_readManyItems_GetAllResources_True()
    {
        $this->_login();
        $makeModelResult = $this->makeModel();
        $readOneItemResult = $makeModelResult->readManyItems();
        $isCorrectReadOneItemResult
            = ($readOneItemResult instanceof Zend_Db_Table_Rowset);
        $this->_logout();
        
        $this->assertTrue($isCorrectReadOneItemResult);
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
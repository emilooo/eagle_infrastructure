<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the method of readManyOtems
 * when try get the resources from database structre they not exist.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Crud_ReadManyItems_NotExistItemsTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_readManyItems_GetAllResources_True()
    {
        $this->_login();
        $makeModelResult = $this->makeModel();
        $readManyItemsResult = $makeModelResult->readManyItems();
        $isCorrectReadOneItemResult
            = ($readManyItemsResult instanceof Zend_Db_Table_Rowset);
        $isCorrectResultCount = ($readManyItemsResult->count() == 0);
        
        $this->assertTrue($isCorrectReadOneItemResult);
        $this->assertTrue($isCorrectResultCount);
    }
    
    public function test_readManyItems_GetPartResources_True()
    {
        $this->_login();
        $makeModelResult = $this->makeModel();
        $readManyItemsResult = $makeModelResult->readManyItems(1);
        $isCorrectReadOneItemResult
            = ($readManyItemsResult instanceof Zend_Paginator);
        $isCorrectResultCount = ($readManyItemsResult->count() == 0);
        
        $this->assertTrue($isCorrectReadOneItemResult);
        $this->assertTrue($isCorrectResultCount);
    }
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the basic interface for testing the method of createItem
 * when access denied
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Crud_CreateItem_AccessDeniedTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_createItem_AccessDenied_Exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $makeModelResult->createItem(
                $this->_getDataToSave()
            );
        } catch (Infrastructure_Acl_AccessDenied $ex) {
            $getMessage = $ex->getMessage();
            $isCorrectMessage = ($getMessage
                === 'You haven\'t permissions to execute this operation!');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the correct values of parameter of dataToSave
     * 
     * @return array
     */
    abstract protected function _getDataToSave();
}
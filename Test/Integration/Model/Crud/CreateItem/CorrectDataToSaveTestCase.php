<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the method of createItem
 * when parameter of dataToSave is correct.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Crud_CreateItem_CorrectDataToSaveTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_createItem_InsertCorrectDataToSave_True()
    {
        $this->_login();
        $makeModelResult = $this->makeModel();
        $createItemResult = $makeModelResult->createItem(
            $this->_getDataToSave()
        );
        $this->_logout();
        $isCorrectItemResult = ($createItemResult > 0);
        
        $this->assertTrue($isCorrectItemResult);
        $this->assertTablesEqual(
            $this->_getExpectedTable(),
            $this->_getActualTable()
        );
    }
    
    /**
     * Returns the correct values of parameter of dataToSave
     * 
     * @return array
     */
    abstract protected function _getDataToSave();
    
    /**
     * @return PHPUnit_Extensions_Database_DataSet_ITable
     */
    abstract protected function _getExpectedTable();
    
    /**
     * @return PHPUnit_Extensions_Database_DataSet_ITable
     */
    abstract protected function _getActualTable();
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
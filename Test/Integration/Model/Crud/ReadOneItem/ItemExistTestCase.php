<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the method of readOneItem
 * when try get the exist resource from database structure
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Crud_ReadOneItem_ItemExistTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_readOneItem_GetExistResource_True()
    {
        $this->_login();
        $makeModelResult = $this->makeModel();
        $readOneItemResult = $makeModelResult->readOneItem(
            $this->_getId()
        );
        $isCorrectReadOneItemResult
            = ($readOneItemResult instanceof Zend_Db_Table_Row_Abstract);
        $this->_logout();
        
        $this->assertTrue($isCorrectReadOneItemResult);
    }
    
    /**
     * Returns the number of id
     * 
     * @abstract
     * @return id
     */
    abstract protected function _getId();
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}


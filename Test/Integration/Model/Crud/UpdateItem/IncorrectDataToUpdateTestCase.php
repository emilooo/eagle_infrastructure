<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the method of createItem
 * when parameter of dataToSave is incorrect.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Crud_UpdateItem_IncorrectDataToUpdateTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_updateItems_InsertIncorrectDataToUpdate_False()
    {
        $this->_login();
        $makeModelResult = $this->makeModel();
        $updateItemsResult = $makeModelResult->updateItem(
            $this->_getId(),
            $this->_getDataToUpdate()
        );
        $this->_logout();
        $isCorrectItemResult = ($updateItemsResult > 0);
        
        $this->assertFalse($isCorrectItemResult);
        $this->assertTablesEqual(
            $this->_getExpectedTable(),
            $this->_getActualTable()
        );
    }
    
    /**
     * Returns the number of id
     * 
     * @abstract
     * @return int
     */
    abstract protected function _getId();
    
    /**
     * Returns the incorrect values of parameter of dataToUpdate
     * 
     * @abstract
     * @return array
     */
    abstract protected function _getDataToUpdate();
    
    /**
     * @return PHPUnit_Extensions_Database_DataSet_ITable
     */
    abstract protected function _getExpectedTable();
    
    /**
     * @return PHPUnit_Extensions_Database_DataSet_ITable
     */
    abstract protected function _getActualTable();
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the method of deleteItems
 * when input parameter of id is invalid and access allow.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Crud_DeleteItem_InvalidIdTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_deleteItems_InvalidId_Exception()
    {
        try {
            $this->_login();
            $makeModelResult = $this->makeModel();
            $makeModelResult->deleteItem(
                $this->_getId()
            );
            $this->_logout();
        } catch (Exception $ex) {
            $getExceptionMessageResult = $ex->getMessage();
            $isCorrectMessage
                = ('assert(): Assertion failed' == $getExceptionMessageResult);
            $this->_logout();
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the invalid id of item
     * 
     * @abstract
     * @return mixed
     */
    abstract protected function _getId();
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic interface for testing the method of deleteItems
 * when try delete exist resource and access allow.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Crud_DeleteItem_ItemExistTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_deleteItems_DeletingExistResource_True()
    {
        $this->_login();
        $makeModelResult = $this->makeModel();
        $deleteItemsResult = $makeModelResult->deleteItem(
            $this->_getId()
        );
        $this->_logout();
        $isCorrectResult = ($deleteItemsResult > 0);
        
        
        $this->assertTrue($isCorrectResult);
        $this->assertTablesEqual(
            $this->_getExpectedTable(),
            $this->_getActualTable()
        );
    }
    
    /**
     * @return PHPUnit_Extensions_Database_DataSet_ITable
     */
    abstract protected function _getExpectedTable();
    
    /**
     * @return PHPUnit_Extensions_Database_DataSet_ITable
     */
    abstract protected function _getActualTable();
    
    /**
     * Returns the correct id of item
     * 
     * @abstract
     * @return int
     */
    abstract protected function _getId();
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
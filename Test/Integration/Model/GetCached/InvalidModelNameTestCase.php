<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Cache
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Implements the basic implementation for testing the method
 * of getCached in the crud model. When parameter of modelName is invalid
 * 
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Integration_Model_GetCached_InvalidModelNameTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getModelClassNameResult();
        
        return $modelInstance;
    }
    
    /**
     * Verify output data when try get cached resource
     */
    public function test_getCached_InvalidModelName_True()
    {
        try {
            $makeModelResoult = $this->makeModel();
            $getCachedResult = $makeModelResoult->getCached(
                111
            );   
        } catch (Exception $exceptions) {
            $getExceptionMessage = $exceptions->getMessage();
            $isCorrectMessage
                = ($getExceptionMessage == 'assert(): Assertion failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
}
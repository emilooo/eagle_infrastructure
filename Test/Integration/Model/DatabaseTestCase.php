<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for database integration testing of crud model
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_DatabaseTestCase
extends Zend_Test_PHPUnit_DatabaseTestCase
{
    /**
     * @var Zend_Test_PHPUnit_Db_Connection 
     */
    private $_connection;
    
    /**
     * @return \PHPUnit_Extensions_Database_Operation
     */
    protected function getSetUpOperation()
    {
        return new \PHPUnit_Extensions_Database_Operation_Composite(
            array(
                \PHPUnit_Extensions_Database_Operation_Factory::DELETE_ALL(),
                \PHPUnit_Extensions_Database_Operation_Factory::INSERT()
            )
        );
    }
    
    /**
     * @return Zend_Test_PHPUnit_Db_Connection
     */
    public function getConnection()
    {
        if ($this->_connection === null) {
            $getConfiguration = $this->_getConfiguration();
            $getConnectionByConfiguration
                = $this->_getConnectionByConfiguration($getConfiguration);
            $this->_connection = $getConnectionByConfiguration;
        }
        
        return $this->_connection;
    }
    
    /**
     * @return Zend_Config_Ini
     */
    private function _getConfiguration()
    {
        $configuration = new Zend_Config_Ini(
            APPLICATION_CONFIGURATION_FILE,
            APPLICATION_ENV
        );
        
        return $configuration;
    }
    
    /**
     * @param Zend_Config_Ini $configuration
     * @return Zend_Test_PHPUnit_Db_Connection
     */
    private function _getConnectionByConfiguration(
        Zend_Config_Ini $configuration
    )
    {
        $connection = Zend_Db::factory(
            $configuration->resources->db
        );
        $createZendDbConnectionResult = $this->createZendDbConnection(
            $connection,
            $configuration->resources->db->params->dbname
        );
        Zend_Db_Table_Abstract::setDefaultAdapter($connection);
        
        return $createZendDbConnectionResult;
    }
    
    /**
     * Retruns the instance of crud model
     * 
     * @return Infrastructure_Model_Crud_Abstract
     */
    abstract public function makeModel();
    
    /**
     * Returns the full name of model class
     * 
     * @return string
     */
    protected abstract function _getModelFullClassName();
    
    /**
     * Returns the short name of model class
     * 
     * @return string
     */
    protected abstract function _getModelShortClassName();
}
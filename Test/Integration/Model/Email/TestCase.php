<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of unit test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Implements the basic implementation for testing the method
 * of sendMessage in the email model. This test verifies the behavior
 * when input parameter of dataToSave is incorrect and mail has sended
 * to file.
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Integration_Model_Email_TestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    /**
     * Returns the information where e-mail files are storing
     * 
     * @return string
     */
    abstract protected function _getEmailTestDirectory();
    
    /**
     * Returns the content of e-mail
     * 
     * @return array
     */
    protected function _readAllEmails()
    {
        $getEmailTestDirectory = $this->_getEmailTestDirectory();
        $getAllFilesFromEmailTestDirectory
            = array_diff(scandir($getEmailTestDirectory), array('..', '.'));
        sort($getAllFilesFromEmailTestDirectory);
        $emails = array();
        foreach ($getAllFilesFromEmailTestDirectory as $file) {
            $email_str = $getEmailTestDirectory . $file;
            $emails[] = new Zend_Mail_Message_File(array('file' => $email_str));
        }
        return $emails;
    }
    
    /**
     * Removes the all emails from test directory
     * 
     * @return void
     */
    protected function _removeAllEmails()
    {
        $directory = $this->_getEmailTestDirectory();
        $files1 = array_diff(scandir($directory), array('..', '.'));
        foreach ($files1 as $val) {
            unlink($directory . "/" . $val);
        }
    }
}
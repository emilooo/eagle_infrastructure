<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model_Email
 * @subpackage SendMessage
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of unit test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Email/'
    . 'TestCase.php';

/**
 * Implements the basic implementation for testing the method
 * of sendMessage in the email model. This test verifies the behavior
 * when input parameter of dataToSave is incorrect. When dataToSend is correct
 * method returns the true
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model_Email
 * @subpackage SendMessage
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Integration_Model_Email_SendMessage_CorrectDataToSendTestCase
extends Infrastructure_Test_Integration_Model_Email_TestCase
{
    /**
     * Returns the incorrect data to send. This return type must be array
     * 
     * @return array
     */
    abstract protected function _getCorrectDataToSend();
    
    /**
     * Returns the name of form
     * 
     * @return Infrastructure_Form_Abstract
     */
    abstract protected function _getFormName();
    
    /**
     * Returns the name of resource class
     * 
     * @return string
     */
    abstract protected function _getFullResourceClassName();
    
    public function setUp()
    {
        parent::setUp();
        
        $this->_removeAllEmails();
    }
    
    public function tearDown()
    {
        parent::tearDown();
        
        $this->_removeAllEmails();
    }
    
    /**
     * Verifies the behavior of method of sendMessage
     * does data to send are invalid
     */
    public function test_insertCorrectDataToSend_false()
    {
        $getConfiguration = $this->getConfiguration();
        $dataToSend = $this->_getCorrectDataToSend();
        
        $makeModelResult = $this->makeModel();
        $sendMessageResult = $makeModelResult->sendMessage($dataToSend);
        $getEmailFromFileResult = $this->_readAllEmails();
        
        $this->assertTrue($sendMessageResult);
        $this->assertTrue((count($getEmailFromFileResult) > 0));
        $this->assertContains(
            $getConfiguration["email"]["params"]["username"],
            $getEmailFromFileResult[0]->getHeader('to')
        );
        $this->assertContains(
            $dataToSend["sender"],
            $getEmailFromFileResult[0]->getHeader('from')
        );
        $this->assertContains(
            $dataToSend["subject"],
            $getEmailFromFileResult[0]->getHeader('subject')
        );
        $this->assertContains(
            $dataToSend["content"],
            $getEmailFromFileResult[0]->getContent()
        );
        
    }
    
    /**
     * Returns the prepared email model
     * 
     * @return Infrastructure_Model_Email
     */
    public function makeModel()
    {
        $fullModelClassName = $this->_getModelFullClassName();
        $modelInstance = new $fullModelClassName(
            array(
                'acl' => $this->_makeAclMock(),
                'identity' => $this->_makeIdentityMock()
            )
        );
        
        return $modelInstance;
    }
    
    /**
     * Returns instance of alc mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        $aclMock
            ->shouldReceive('isAllowed')
            ->andReturn(true);
        
        return $aclMock;
    }
    
    /**
     * Returns the instance of acl role mock
     * 
     * @return Zend_Acl_Role_Interface
     */
    private function _makeIdentityMock()
    {
        $identityMock = Mockery::mock('Zend_Acl_Role_Interface');
        
        return $identityMock;
    }
}
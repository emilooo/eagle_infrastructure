<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of unit test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Implements the basic implementation for testing the method
 * of sendMessage in the email model. This test verifies the behavior
 * when input parameter of dataToSave is invalid. When dataToSend is invalid
 * method throws the exception
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Integration_Model_Email_SendMessage_InvalidDataToSendTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    /**
     * Returns the name of form
     * 
     * @return Infrastructure_Form_Abstract
     */
    abstract protected function _getFormName();
    
    /**
     * Returns the invalid data to send. This return type can't be array
     * 
     * @return string|int
     */
    abstract protected function _getInvalidDataToSend();
    
    /**
     * Vierifies the behavior of method of sendMessage is data to send
     * are invalid
     */
    public function test_insertInvalidDataToSend_exception()
    {
        try {
            $dataToSend = $this->_getInvalidDataToSend();
            $model = $this->makeModel();
            $sendMessageResult = $model->sendMessage($dataToSend);
        } catch (Exception $ex) {
            $getMessageResult = $ex->getMessage();
            
            $isCorrectMessage = ($getMessageResult
                === 'Zend_Form::isValid expects an array');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    public function makeModel()
    {
        $fullModelClassName = $this->_getModelFullClassName();
        $modelInstance = new $fullModelClassName(
            array(
                'acl' => $this->_makeAclMock(),
                'identity' => $this->_makeIdentityMock()
            )
        );
        
        return $modelInstance;
    }
    
    /**
     * Returns instance of alc mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        $aclMock
            ->shouldReceive('isAllowed')
            ->andReturn(true);
        
        return $aclMock;
    }
    
    /**
     * Returns the instance of acl role mock
     * 
     * @return Zend_Acl_Role_Interface
     */
    private function _makeIdentityMock()
    {
        $identityMock = Mockery::mock('Zend_Acl_Role_Interface');
        
        return $identityMock;
    }
}
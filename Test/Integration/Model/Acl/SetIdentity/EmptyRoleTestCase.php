<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the interface for testing the behavior when try sets
 * the identity when property of role is empty
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Acl_SetIdentity_EmptyRoleTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $model = new $getModelFullClassNameResult();
        
        return $model;
    }
    
    public function test_setIdentity_ColumnUserRoleEmpty_True()
    {
        try {
            $makeModelResult = $this->makeModel();
            $setIdentityResult = $makeModelResult->setIdentity(
                $this->_getIdentity()
            );
        } catch (Exception $exceptions) {
            $getExceptionMessageResult = $exceptions->getMessage();
            $isCorrectException = ($getExceptionMessageResult
                === 'assert(): Assertion failed');
            
            return $this->assertTrue($isCorrectException);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the instance of row
     * 
     * @return Zend_Db_Table_Row
     */
    abstract protected function _getIdentity();
}
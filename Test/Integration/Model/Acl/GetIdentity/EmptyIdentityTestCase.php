<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try gets the identity when property of identityRole is not defined.
 * 
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Integration_Model_Acl_GetIdentity_EmptyIdentityTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $model = new $getModelFullClassNameResult();
        
        return $model;
    }
    
    public function test_getIdentity_IdentityNotDefined_True()
    {
        $model = $this->makeModel();
        $getIdentityResult = $model->getIdentity();
        $isCorrectIdentityResult = ($getIdentityResult === 'Guest');
        
        $this->assertTrue($isCorrectIdentityResult);
    }
}


<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when try gets the identity when user has logged.
 * 
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Integration_Model_Acl_GetIdentity_ExistIdentityTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $model = new $getModelFullClassNameResult();
        
        return $model;
    }
    
    public function test_getIdentity_UserLogged_True()
    {
        $this->_loginUser();
        $model = $this->makeModel();
        $getIdentityResult = $model->getIdentity();
        $isCorrectIdentityResult = ($getIdentityResult
            instanceof Zend_Acl_Role_Interface);
        $this->_logoutUser();
        
        $this->assertTrue($isCorrectIdentityResult);
    }
    
    /**
     * Login user
     * 
     * @return Void
     */
    abstract protected function _loginUser();
    
    /**
     * Logout user
     * 
     * @return void
     */
    abstract protected function _logoutUser();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when checks the privilage to access denied action
 * 
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Integration_Model_Acl_CheckAcl_AccessToActionIsAllowTestCase
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $model = new $getModelFullClassNameResult();
        
        return $model;
    }
    
    public function test_checkAcl_InsertActionThatAccessAllow_True()
    {
        $this->_login();
        $makeModelResult = $this->makeModel();
        $checkAclResult = $makeModelResult->checkAcl(
            $this->_getActionName()
        );
        $this->_logout();
        
        $this->assertTrue($checkAclResult);
    }
    
    /**
     * Returns the name of action that access is allow
     */
    abstract protected function _getActionName();
    
    /**
     * Login user
     */
    abstract protected function _login();
    
    /**
     * Logout user
     */
    abstract protected function _logout();
}
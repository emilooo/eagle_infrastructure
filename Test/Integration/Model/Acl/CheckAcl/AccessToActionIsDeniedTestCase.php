<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the basic implementation for testing the behavior
 * when checks the privilage to access denied action
 * 
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Test_Integration_Model_Acl_CheckAcl_AccessToActionIsDeniedTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassNameResult = $this->_getModelFullClassName();
        $model = new $getModelFullClassNameResult();
        
        return $model;
    }
    
    public function test_checkAcl_InsertActionThatAccessDenied_False()
    {
        $makeModelResult = $this->makeModel();
        $checkAclResult = $makeModelResult->checkAcl(
            $this->_getActionName()
        );
        
        $this->assertFalse($checkAclResult);
    }
    
    /**
     * Returns the name of action that access is denied
     */
    abstract protected function _getActionName();
}

<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for integration testing of crud model
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_TestCase
extends PHPUnit_Framework_TestCase
{
    /**
     * Retruns the instance of crud model
     * 
     * @return Infrastructure_Model_Crud_Abstract
     */
    abstract public function makeModel();
    
    /**
     * Returns the full name of model class
     * 
     * @return string
     */
    protected abstract function _getModelFullClassName();
    
    /**
     * Returns the short name of model class
     * 
     * @return string
     */
    protected abstract function _getModelShortClassName();
    
    /**
     * Returns the information about application configuration
     * 
     * @return array
     */
    public function getConfiguration()
    {
        $configuration = Zend_Registry::get('config');
        
        return $configuration;
    }
}
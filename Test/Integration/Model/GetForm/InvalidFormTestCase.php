<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the interface for testing the behavior when try gets
 * the form through invalid name of form
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_GetForm_InvalidFormTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getFullModelClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getFullModelClassNameResult();
        
        return $modelInstance;
    }
    
    public function test_getForm_InsertInvalidFormName_Exception()
    {
        try {
            $makeModelResult = $this->makeModel();
            $makeModelResult->getForm(
                $this->_getFormName()
            );
        } catch (Exception $exceptions) {
            $getExceptionMessage = $exceptions->getMessage();
            $isCorrectMessage = ($getExceptionMessage
                == 'assert(): Parameter: formName is bad! failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Retruns the name of form
     * 
     * @return string
     */
    abstract protected function _getFormName();
}
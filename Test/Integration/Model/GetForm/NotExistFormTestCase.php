<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the interface for testing the behavior when try gets
 * the not exist form through correct name of form
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_GetForm_NotExistFormTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getFullModelClassNameResult = $this->_getModelFullClassName();
        $modelInstance = new $getFullModelClassNameResult();
        
        return $modelInstance;
    }
    
    /**
     * @expectedException Infrastructure_Model_InvalidOperation
     */
    public function test_getForm_InsertNotExistFormName_Exception()
    {
        $makeModelResult = $this->makeModel();
        $makeModelResult->getForm(
            $this->_getFormName()
        );
    }
    
    /**
     * Retruns the name of form
     * 
     * @return string
     */
    abstract protected function _getFormName();
}
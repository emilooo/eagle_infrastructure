<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'TestCase.php';

/**
 * Provides the basic interface for testing the method of registerItem
 * when input parameter of dataToRegister is invalid
 * 
 * @abstract
 * @category Test
 * @package Test_Infrastructure_Integration_Model
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Test_Integration_Model_Auth_RegisterItem_InvalidDataTestCase
extends Infrastructure_Test_Integration_Model_TestCase
{
    public function makeModel()
    {
        $getModelFullClassName = $this->_getModelFullClassName();
        $model = new $getModelFullClassName();
        
        return $model;
    }
    
    public function test_registerItem_invalidDataToRegister_true()
    {
        try {
            $makeModelResult = $this->makeModel();
            $makeModelResult->registerItem(
                $this->_getDataToRegister()
            );
        } catch (Exception $ex) {
            $getExceptionMessageResult = $ex->getMessage();
            $isCorrectMessage
                = ('Zend_Form::isValid expects an array'
                    == $getExceptionMessageResult);
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    /**
     * Returns the invalid data to register
     * 
     * @abstract
     * @return array
     */
    abstract protected function _getDataToRegister();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides interface for all controllers in application
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Controller_Interface
{
    /**
     * Prepares settings of controller
     */
    public function init();
    
    /**
     * Verifies if this request is authorized
     * 
     * @return boolean
     */
    public function isAuthorizedRequest();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for auth controller
 * 
 * @category Infrastructure
  * @package Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Controller_Http_Auth_Interface
extends Infrastructure_Controller_Http_Interface
{
    /**
     * Action login user
     */
    public function loginAction();
    
    /**
     * Action logout user
     */
    public function logoutAction();
    
    /**
     * Action to register new user
     */
    public function registerAction();
    
    /**
     * Action to remind password of user
     */
    public function remindAction();
    
    /**
     * Action which showing the information about current profile of user
     */
    public function profileAction();
}
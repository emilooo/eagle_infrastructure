<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the primary implementation for auth http controllers
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Controller_Http
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Controller_Http_Auth_Abstract
extends Infrastructure_Controller_Http_Crud_Abstract
implements Infrastructure_Controller_Http_Auth_Interface
{
    public function init()
    {
        parent::init();
        
        $this->view->userLogin
            = $this->_helper->getHelper('Acl')->getLogin();
        $this->view->userIdentity
            = $this->_helper->getHelper('Acl')->getIdentity();
    }
    
    public function loginAction()
    {
        $getLoginActionResult = $this->_getAction('Login');
        if (!$getLoginActionResult->hasAccessToAction()) {
            return $getLoginActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($this->_helper->getHelper('Acl')->getIdentity() != 'Guest'
                || $getLoginActionResult->makeAction()) {
                return $getLoginActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getLoginActionResult->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
    
    public function logoutAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $getLogoutActionResult = $this->_getAction('Logout');
        if (!$getLogoutActionResult->hasAccessToAction()) {
            return $getLogoutActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getLogoutActionResult->makeAction()) {
                return $getLogoutActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getLogoutActionResult->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
    
    public function registerAction()
    {
        $getRegisterActionResult = $this->_getAction('Register');
        if (!$getRegisterActionResult->hasAccessToAction()) {
            return $getRegisterActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getRegisterActionResult->makeAction()) {
                return $getRegisterActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getRegisterActionResult
                    ->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
    
    public function remindAction()
    {
        $getRemindActionResult = $this->_getAction('Remind');
        if (!$getRemindActionResult->hasAccessToAction()) {
            return $getRemindActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getRemindActionResult->makeAction()) {
                return $getRemindActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getRemindActionResult->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
    
    public function profileAction()
    {
        
        $getProfileActionResult = $this->_getAction('Profile');
         if (!$getProfileActionResult->hasAccessToAction()) {
            return $getProfileActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getProfileActionResult->makeAction()) {
                return $getProfileActionResult
                    ->doWhenActionSuccessfullyMaked();
            } else {
                return $getProfileActionResult
                    ->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
}
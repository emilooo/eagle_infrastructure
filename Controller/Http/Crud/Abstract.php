<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Infrastructure_Controller_Http_Crud_Abstract
 * 
 * Provides the primary implementation for crud http controllers
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Controller_Http_Crud_Abstract
extends Infrastructure_Controller_Http_Abstract
implements Infrastructure_Controller_Http_Crud_Interface
{
    public function indexAction()
    {
        $getIndexActionResult = $this->_getAction('Index');
        if (!$getIndexActionResult->hasAccessToAction()) {
            return $getIndexActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getIndexActionResult->makeAction()) {
                return $getIndexActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getIndexActionResult->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
    
    public function createAction()
    {
        $getCreateActionResult = $this->_getAction('Create');
        if (!$getCreateActionResult->hasAccessToAction()) {
            return $getCreateActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getCreateActionResult->makeAction()) {
                return $getCreateActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getCreateActionResult->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
    
    public function readAction()
    {
        $getReadActionResult = $this->_getAction('Read');
        if (!$getReadActionResult->hasAccessToAction()) {
            return $getReadActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getReadActionResult->makeAction()) {
                return $getReadActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getReadActionResult->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
    
    public function updateAction()
    {
        $getUpdateActionResult = $this->_getAction('Update');
        if (!$getUpdateActionResult->hasAccessToAction()) {
            return $getUpdateActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getUpdateActionResult->makeAction()) {
                return $getUpdateActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getUpdateActionResult->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
    
    public function deleteAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $getDeleteActionResult = $this->_getAction('Delete');
        if (!$getDeleteActionResult->hasAccessToAction()) {
            return $getDeleteActionResult->doWhenAccessToActionIsDenied();
        } else {
            if ($getDeleteActionResult->makeAction()) {
                return $getDeleteActionResult->doWhenActionSuccessfullyMaked();
            } else {
                return $getDeleteActionResult->doWhenActionUnsuccessfullyMaked();
            }
        }
    }
}
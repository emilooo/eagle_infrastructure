<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for crud controller
 * 
 * @category Infrastructure
 * @package Infrastructure_Controller_Http
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Controller_Http_Crud_Interface
extends Infrastructure_Controller_Http_Interface
{
    /**
     * Action renders all items
     */
    public function indexAction();
    
    /**
     * Action creates the new item
     */
    public function createAction();
    
    /**
     * Action reads the exist item
     */
    public function readAction();
    
    /**
     * Action updates the exist item
     */
    public function updateAction();
    
    /**
     * Action deletes the exist item
     */
    public function deleteAction();
    
    /**
     * Returns the instance of model
     * 
     * @return Infrastructure_Model_Crud_Interface
     */
    public function getModel();
}
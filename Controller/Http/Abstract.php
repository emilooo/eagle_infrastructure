<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the primary implementation for http controllers
 * 
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Controller_Http_Abstract
extends Zend_Controller_Action
implements Infrastructure_Controller_Http_Interface
{
    /**
     * Stores the created instance of actions
     * 
     * @var array
     */
    protected $_actions = array();
    
    public function init()
    {
        $this->setPageTemplate($this->view->getHelper('Layout'));
        $this->setPageMetaTags($this->view->getHelper('HeadMeta'));
        $this->setPageStyles($this->view->getHelper('HeadStyle'));
        $this->setPageLinks($this->view->getHelper('HeadLink'));
        $this->setPageScripts($this->view->getHelper('HeadScript'));
        $this->setPageTitle($this->view->getHelper('HeadTitle'));
        $this->setPageBreadcrumbs();
    }
    
    /**
     * Verifies if request is authorized
     * 
     * @return boolean
     */
    public function isAuthorizedRequest()
    {
        return true;
    }
    
    /**
     * Throws the exception for unauthorized request
     * 
     * @throws Infrastructure_Exception_Controller_UnauthorizeRequest
     */
    protected function _throwUnauthorizedRequest()
    {
        throw new Infrastructure_Exception_Controller_UnauthorizedRequest(
            'This request is unauthorized!'
        );
    }
    
    /**
     * Returns the instance of action through short name of action class.
     * If this action will be created it then will be saved in the list
     * 
     * @param string $actionName
     * @return Infrastructure_Action_Interface
     */
    protected function _getAction($actionName)
    {
        if (!array_key_exists($actionName, $this->_actions)) {
            $actionFactoryInstance
                    = Infrastructure_Action_Factory::getInstance();
            $actionInstance = $actionFactoryInstance->make($actionName, $this);
            $this->_actions[$actionName] = $actionInstance;
            
            return $this->_actions[$actionName];
        }
        
        return $this->_actions[$actionName];
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for all http controllers
 * 
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Http
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Controller_Http_Interface
extends Infrastructure_Controller_Interface
{
    /**
     * Adds the name of page template
     * 
     * @param Zend_View_Helper_Layout $layoutHelper Instance of zend
     * layout helper
     * @return void
     */
    public function setPageTemplate(Zend_View_Helper_Layout $layoutHelper);
    
    /**
     * Adds the page meta tags
     * 
     * @param Zend_View_Helper_HeadMeta $headMetaHelper Instance of head meta
     * @return void
     */
    public function setPageMetaTags(Zend_View_Helper_HeadMeta $headMetaHelper);
    
    /**
     * Adds the page css styles
     * 
     * @param Zend_View_Helper_HeadStyle $headStyleHelper Instance of head style
     * @return void
     */
    public function setPageStyles(Zend_View_Helper_HeadStyle $headStyleHelper);
    
    /**
     * Adds the page css links
     * 
     * @param Zend_View_Helper_HeadLink $headLinkHelper Instance of head link
     * helper
     * @return void
     */
    public function setPageLinks(Zend_View_Helper_HeadLink $headLinkHelper);
    
    /**
     * Adds the page java scripts
     * 
     * @param Zend_View_Helper_HeadScript $headScriptHelper Instance
     * of head script
     * @return void
     */
    public function setPageScripts(
        Zend_View_Helper_HeadScript $headScriptHelper
    );
    
    /**
     * Adds the page title
     * 
     * @param Zend_View_Helper_HeadTitle $headTitleHelper Instance of title
     * helper
     * @return void
     */
    public function setPageTitle(Zend_View_Helper_HeadTitle $headTitleHelper);
    
    /**
     * Adds the page breadcrumbs
     */
    public function setPageBreadcrumbs();
}
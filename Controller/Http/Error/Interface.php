<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Http
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the primary interface for error handling
 * 
 * @package Infrastructure_Controller_Http
 * @subpackage Error
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Controller_Http_Error_Interface
extends Infrastructure_Controller_Http_Interface
{
    /**
     * Initiates the process of handling exception
     */
    public function errorAction();
    
    /**
     * Returns the instance of exception
     * 
     * @return Infrastructure_Exception_Base
     */
    public function getException();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for all controllers of rest crud
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Controller_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
interface Infrastructure_Controller_Rest_Crud_Interface
extends Infrastructure_Controller_Rest_Interface
{
    /**
     * Returns the instance of crud model
     * 
     * @return Infrastructure_Model_Crud_Interface
     */
    public function getModel();
}
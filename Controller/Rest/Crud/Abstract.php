<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for all controllers of rest crud
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Controller_Rest
 * @subpackage Crud
 * @author Emil Maraszek <programmer@emilooo.pl>
 * @since 2.8
 */
abstract class Infrastructure_Controller_Rest_Crud_Abstract
extends Infrastructure_Controller_Rest_Abstract
implements Infrastructure_Controller_Rest_Crud_Interface
{
    public function indexAction()
    {
        try {
            $getActionResult = $this->_getAction('Index');
            if ($getActionResult->makeAction()) {
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $this->getResponse()->setHttpResponseCode(400);
            }
            
            echo $getActionResult->getResponse();
        } catch (Infrastructure_Acl_AccessDenied $ex) {
            $this->getResponse()->setHttpResponseCode(403);
        } catch (Exception $ex) {
            $this->getResponse()->setHttpResponseCode(406);
        }
    }
    
    /**
     * @todo Create the special error handler for handling the exceptions
     * through controller of rest. This handler must handling the exceptions
     * and changing they on the http response code.
     */
    public function getAction()
    {
        try {
            $getActionResult = $this->_getAction('Get');
            if ($getActionResult->makeAction()) {
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $this->getResponse()->setHttpResponseCode(404);
            }
            
            echo $getActionResult->getResponse();
        } catch (Infrastructure_Acl_AccessDenied $ex) {
            $this->getResponse()->setHttpResponseCode(403);
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            $this->getResponse()->setHttpResponseCode(404);
        } catch (Exception $ex) {
            $this->getResponse()->setHttpResponseCode(406);
        }
    }
    
    public function putAction()
    {
        try {
            $getActionResult = $this->_getAction('Put');
            if ($getActionResult->makeAction()) {
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $this->getResponse()->setHttpResponseCode(406);
            }
            
            echo $getActionResult->getResponse();
        } catch (Infrastructure_Acl_AccessDenied $ex) {
            return $this->getResponse()->setHttpResponseCode(403);
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            return $this->getResponse()->setHttpResponseCode(404);
        } catch (Exception $ex) {
            return $this->getResponse()->setHttpResponseCode(406);
        }
    }
    
    public function postAction()
    {
        try {
            $getActionResult = $this->_getAction('Post');
            if ($getActionResult->makeAction()) {
                $this->getResponse()->setHttpResponseCode(201);
            } else {
                $this->getResponse()->setHttpResponseCode(406);
            }
            
            echo $getActionResult->getResponse();
        } catch (Infrastructure_Acl_AccessDenied $ex) {
            return $this->getResponse()->setHttpResponseCode(403);
        } catch (Exception $ex) {
            return $this->getResponse()->setHttpResponseCode(406);
        }
    }
    
    public function deleteAction()
    {
        try {
            $getActionResult = $this->_getAction('Delete');
            if ($getActionResult->makeAction()) {
                $this->getResponse()->setHttpResponseCode(200);
                echo $getActionResult->getResponse();
            }
        } catch (Infrastructure_Acl_AccessDenied $ex) {
            return $this->getResponse()->setHttpResponseCode(403);
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            return $this->getResponse()->setHttpResponseCode(404);
        } catch (Exception $ex) {
            return $this->getResponse()->setHttpResponseCode(406);
        }
    }
}
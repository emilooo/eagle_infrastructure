<?php
/**
 * @category Infrastructure
 * @package Infrastructure_Controller_Rest
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for rest controllers which supports
 * the cooperate with email services
 * 
 * @abstract
 * @package Infrastructure_Controller_Rest
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Controller_Rest_Email_Abstract
extends Infrastructure_Controller_Rest_Abstract
{
    public function postAction()
    {
        $postAction = $this->_getAction('Post');
        $makePostActionResult = $postAction->makeAction();
        $actionJsonResponse = $postAction->getResponse();
        $actionStdResponse = json_decode($postAction->getResponse());
            assert(isset($actionStdResponse->code));
        $this->getResponse()->setHttpResponseCode($actionStdResponse->code);
        $this->getResponse()->setBody($actionJsonResponse);
    }
}
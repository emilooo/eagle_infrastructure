<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Rest
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface to manage
 * the resources with the level of the front-end layer
 * 
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Rest
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Controller_Rest_Interface
extends Infrastructure_Controller_Interface
{
    /**
     * Gets the resources data
     * 
     * If success, returns the 200 code, if defeat returns the 400 code
     * 
     * @return void
     */
    public function indexAction();
    
    /**
     * Gets the resource data
     * 
     * If success, returns the 200 code, if defeat returns the 400 code
     * 
     * @return void
     */
    public function getAction();
    
    /**
     * Updates the resource data
     * 
     * If success, returns the 200 code, if defeat returns the 400 code
     * 
     * @return void
     */
    public function putAction();
    
    /**
     * Creates the resource
     * 
     * If success, returns the 201 code, if defeat returns the 400 code
     * 
     * @return void
     */
    public function postAction();
    
    /**
     * Deletes the resource through id
     * 
     * If success, returns the 204 code, if defeat returns the 400 code
     * 
     * @return void
     */
    public function deleteAction();
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Rest
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the primary implementation for auth http controllers
 * 
 * @abstract
 * @category Infrastructure
 * @package Infrastructure_Controller_Rest
 * @subpackage Auth
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Controller_Rest_Auth_Abstract
extends Infrastructure_Controller_Rest_Crud_Abstract
implements Infrastructure_Controller_Rest_Auth_Interface
{
    public function init()
    {
        parent::init();
    }
    
    public function loginAction()
    {
        $getLoginAction = $this->_getAction('Login');
        try {
            $getLoginAction->makeAction();
            echo $getLoginAction->getResponse();
        } catch (Exception $ex) {
            $this->getResponse()->setHttpResponseCode(406);
            echo $getLoginAction->getResponse();
        }
    }
    
    public function logoutAction()
    {
        $getLogoutAction = $this->_getAction('Logout');
        try {
            $getLogoutAction->makeAction();
            echo $getLogoutAction->getResponse();
        } catch (Exception $ex) {
            $this->getController()->getResponse()->setHttpResponseCode(406);
            echo $getLogoutAction->getResponse();
        }
    }
    
    public function registerAction()
    {
    }
    
    public function remindAction()
    {
    }
}
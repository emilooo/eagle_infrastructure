<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller_Rest
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for error handling
 * 
 * @abstract
 * @package Infrastructure_Controller_Rest
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Controller_Rest_Error_Abstract
extends Infrastructure_Controller_Rest_Abstract
implements Infrastructure_Controller_Rest_Error_Interface
{
    /**
     * Stores the registrated error handlers
     * 
     * @var array
     */
    protected $_registeredExceptionHandlerList = array();
    
    /**
     * Initiates the process of handling exception
     * 
     * Steps:
     * - gets the name of exception
     * - verifies if exception is registered then executes expected action
     * - if exception is not registered then executes the default
     *   action of ApplicationError
     * 
     * @return boolean if all is ok returns true else false
     */
    public function errorAction()
    {
        $exceptionClassName = get_class($this->getException());
        if ($this->_isExceptionHandler($exceptionClassName)) {
            $getExceptionHandler
                = $this->_getExceptionHandler($exceptionClassName);
            $action = $this->_getAction($getExceptionHandler['action']);
            return $action->makeAction();
        }
        
        $action = $this->_getAction("ApplicationError");
        return $action->makeAction();
    }
    
    /**
     * Returns the instance of exception
     * 
     * @return Infrastructure_Exception_Base
     */
    public function getException()
    {
        $getExceptionsResult = $this->_getParam('error_handler');
        
        return $getExceptionsResult['exception'];
    }
    
    /**
     * @param Infrastructure_Exception_Base $exceptionClassName
     * @return boolean If handler exist returns true else false
     */
    protected function _isExceptionHandler($exceptionClassName)
    {
        $isCorrectClassName = is_string($exceptionClassName);
            assert($isCorrectClassName);
        
        $handlerExist = array_key_exists(
            $exceptionClassName,
            $this->_registeredExceptionHandlerList
        );
        
        return $handlerExist;
    }
    
    /**
     * Registers exception handler
     * 
     * @param string $exceptionClassName Full class name of exception
     * @param string $actionClassName Short action name
     * @param array $optionList
     * @return boolean
     */
    protected function _registerExceptionHandler(
        $exceptionClassName, $actionClassName, $optionList = array()
    )
    {
        $isCorrectActionClassName = is_string($actionClassName);
            assert($isCorrectActionClassName);
            
        $isCorrectOptionList = is_array($optionList);
            assert($isCorrectOptionList);
        
        $handlerExist = $this->_isExceptionHandler($exceptionClassName);
        if (!$handlerExist) {
            $this->_registeredExceptionHandlerList[$exceptionClassName] = array(
                'action' => $actionClassName,
                'options' => $optionList
            );
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Returns the registered exception handler
     * 
     * Example of response:
     * array('action' => 'ApplicationError', 'options' => array());
     * 
     * 
     * @param string $exceptionClassName Full exception class name
     * @return array|boolean
     */
    protected function _getExceptionHandler($exceptionClassName)
    {
        $existHandler = $this->_isExceptionHandler($exceptionClassName);
        if ($existHandler) {
            return $this->_registeredExceptionHandlerList[$exceptionClassName];
        }
        
        return false;
    }
    
    /**
     * Removes the registered exception handler
     * 
     * @param string $exceptionClassName Full exception class name
     * @return boolean
     */
    protected function _removeExceptionHandler($exceptionClassName)
    {
        $existHandler = $this->_isExceptionHandler($exceptionClassName);
        if ($existHandler) {
            unset($this->_registeredExceptionHandlerList[$exceptionClassName]);
            return true;
        }
        
        return false;
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Rest
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the basic implementation for all controllers of rest
 * 
 * @category Infrastructure
 * @package Infrastructure_Controller
 * @subpackage Rest
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Infrastructure_Controller_Rest_Abstract
extends Zend_Rest_Controller
implements Infrastructure_Controller_Rest_Interface
{
    /**
     * Stores register instance of controller actions
     * 
     * @var Infrastructure_Action_Interface
     */
    protected $_actions = array();
    
    public function init()
    {
        parent::init();
        
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }
    
    public function isAuthorizedRequest()
    {
    }
    
    /**
     * Returns the instance of action through short name of action class.
     * If this action will be created it then will be saved in the list
     * 
     * @param string $actionName
     * @return Infrastructure_Action_Interface
     */
    protected function _getAction($actionName)
    {
        if (!array_key_exists($actionName, $this->_actions)) {
            $actionFactoryInstance
                    = Infrastructure_Action_Factory::getInstance();
            $actionInstance = $actionFactoryInstance->make($actionName, $this);
            $this->_actions[$actionName] = $actionInstance;
            
            return $this->_actions[$actionName];
        }
        
        return $this->_actions[$actionName];
    }
}
<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Access denied exception
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Infrastructure_Acl_AccessDenied
extends Infrastructure_Exception_Base
{
}
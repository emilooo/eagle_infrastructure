<?php
/**
 * This source file is part of content management system
 *
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides interface for all implementations of application
 * access control lists
 * 
 * @category Infrastructure
 * @package Infrastructure
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
interface Infrastructure_Acl_Interface
{
}